<!-- this is 404.php -->
<?php get_header(); ?>

<?php get_template_part( 'template-parts/all', 'hero_banner' ); ?>

<!-- выводим тело страницы -->

<div class="row column margin-tb-l">
<p>Страница, на которую вы перешли не найдена на сайте.</p>
<p>Попробуйте начать с <a href="<?php bloginfo('url') ?>">главной страницы</a> или воспользуйтесь поиском по сайту:</p>
<?php get_search_form(); ?>
</div>



<?php
get_sidebar();
get_footer();
