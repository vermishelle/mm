<!-- this is archive-mm_branch.php -->

<?php get_header(); ?>

<nav class="row column padding-b-l">
  <?php custom_breadcrumbs(); ?>
</nav>

  <?php $org = new Mm_org(); ?>


	<?php do_action('mm_show_org', $org); ?>


  <?php $branches_arr = $org->branches(); ?>

  <section>
    <div class='row column'>
      <h2>Подразделения</h2>
    </div>
    <div class='row'>
      <?php foreach ($branches_arr as $branch): ?>
        <div class="column small-12 medium-6 margin-b">
          <a href="<?php echo $branch->url() ?>">
            <?php echo $branch->thumbnail()->show("full-width", "16x9-s"); ?>
            <div class="bg-accent text-center white-text padding">
              <p class="margin-b-0"><strong class="uppercase"><?php echo $branch->title() ?></strong></p>
              <p class="margin-b-0"><i><?php echo $branch->adress()?></i></p>
            </div>
          </a>
        </div> <!-- плитка филиалов -->
      <?php endforeach; ?>
    </div>
  </section>

<?php
get_sidebar();
get_footer();
