<!-- this is archive-mm_department.php -->

<?php get_header(); ?>

<?php get_template_part( 'template-parts/all', 'hero_banner' ); ?>

<div class="row column">
  <?php custom_breadcrumbs(); ?>

  <div class="row margin-tb-l"> <!-- внутри будут плитки филиалов -->

    <?php

    $org = new Mm_org();

    $deps = $org->departments();

    foreach ($deps as $dep) {
      if ($dep->hide_from_lists() or !$dep->is_a_root()) {
        continue;
      } ?>

      <div class='column small-6 medium-4 large-3'>

        <a class="black-text" href="<?php echo $dep->url(); ?>">
          <?php echo $dep->thumbnail()->show("full-width", "4x3-s"); // арг? ?>
          <div class="bg-accent padding text-center margin-b-l">
            <p class="uppercase white-text"><?php echo $dep->title(); ?></p>
          </div>

        </a>
      </div>

    <?php }

wp_reset_postdata();     // Restore original Post Data ?>

  </div>

</div>

<?php
get_sidebar();
get_footer();
