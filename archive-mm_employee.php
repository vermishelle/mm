<!-- this is archive-mm_employee.php -->


<?php
$mode = get_theme_mod('employees_display_on_archive_page_mode');
$mm_org = new Mm_org();

// перебираем филиалы. сначала в филиале показываем администрацию, потом -
// сотрудников

?>
<?php get_header(); ?>
<?php get_template_part( 'template-parts/all', 'hero_banner' ); ?>

<div class='bg-grey'>
  <div class='row column small-12 padding-top'>
    <?php custom_breadcrumbs(); ?>
  </div>
  <?php foreach ($mm_org->branches() as $branch) { ?>
    <?php if ($mode != 'doctors_only') {
      mm_show_branch_admins($branch);
    } ?>
    <?php if ($mode != 'admins_only') {
      mm_show_branch_doctors($branch);
    } ?>
  <?php }; ?>


<?php // тут какой-то hardcoded-косяк ниже

   if (!empty(get_theme_mod('empl_full_list_page'))): ?>
      <div class="row column"> <!-- текст под иконками услуг -->
        <a class="button button-with-icon--left padding-top-xl" href="<?php echo bloginfo('url')?>/spisok-sotrudnikov/"><i class="fa fa-search fa-fw"></i> Посмотреть полный список сотрудников</a>
      </div>
    <?php endif; ?>
</div> <!-- bg-grey -->
<?php
get_sidebar();
get_footer();
