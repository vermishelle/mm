<?php get_header();

$org = new Mm_org(); ?>

<?php get_template_part( 'template-parts/all', 'hero_banner' ); ?>
<div class="bg-grey">
  <div class="row">
    <div class="column small-12">
      <?php custom_breadcrumbs(); ?>
    </div>
  </div>

  <?php // если абсолютно все услуги не привязаны к отделениям - форсируем режим отображения услуг вкучу, не по отделениям
  if ( sizeof($org->services_without_departments()) == sizeof ($org->services()) ):
    $force_view_without_deps = true;
  else:
    $force_view_without_deps = false;
  endif; ?>

  <?php if (get_theme_mod('split_by_departments') and !$force_view_without_deps ): // если делим по отделениям
    $deps = $org->departments();
    foreach ($deps as $dep):
      if (!$dep->is_a_root() or empty($dep->services())): continue; endif; ?>
      <div class="row column">
        <h2><?php echo $dep->title() ?></h2>
      </div>
      <?php mm_show_services_in_circles($dep->services());
    endforeach;

    // а тут еще покажем услуги без отделений, если такие есть
    // если вообще все услуги без отделений - по сути надо как ниже

    if (!empty($org->services_without_departments())): ?>
      <div class="row column">
        <h2>Прочие услуги</h2>
      </div>
      <?php mm_show_services_in_circles($org->services_without_departments());
    endif;

  else: // если услуги не нужно делить по отделениям
    $services = $org->services();
    mm_show_services_in_circles($services);
  endif; ?>
</div>

<?php
get_sidebar();
get_footer();
