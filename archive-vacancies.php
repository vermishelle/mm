<!-- archive-vacancies.php -->
<?php get_header();

get_template_part( 'template-parts/all', 'hero_banner' ); ?>

<!-- выводим тело страницы -->

<div class="row padding-b-l">
<div class="column small-12">
  <?php custom_breadcrumbs(); ?>
</div>

<?php if(!empty(get_theme_mod('hr'))): ?>
  <div class="column small-12 medium-8">
<?php else: ?>
  <div class="column small-12">
<?php endif; ?>
  <?php if (have_posts()) : ?>

    <p>В ГП66 открыты следующие вакансии:</p>
    <ul>

    <?php while (have_posts()) :
      the_post(); ?>
        <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
    <?php endwhile; ?>

    </ul>

  <?php else: ?>
      <p><i>Открытых вакансий в данный момент нет.</i></p>

  <?php endif;

  echo get_theme_mod('vacancies_text'); ?>
</div>

<?php if(!empty(get_theme_mod('hr'))): ?>
  <div class="column small-12 medium-4">
    <?php $hr = new Mm_employee(get_theme_mod('hr')); ?>
      <a class="black-text" href="<?php echo $hr->url() ?>">
      <?php $hr->show_card(); // mm_thumbnail_with_placeholder_if_no_thumbnail("3x4", "full-width"); ?>
      <p class="text-center"><strong><?php echo($hr->title()) ?></strong></p>
      <p class="text-center"><?php echo($hr->position()) ?></p>
      </a>
  </div> <!-- колонка с начальником отдела кадров -->
<?php endif; ?>

</div> <!-- колонка со списком вакансий -->



<?php
get_sidebar();
get_footer();
