<!-- this is archive.php -->

<?php get_header(); ?>

<div class="row column">

  <?php custom_breadcrumbs();
  if ($post) { // закрытие условия в самом низу

    if  ($post->post_type == 'mm_employee'): ?>
      <h1>Наши сотрудники</h1>
    <?php endif; ?>

  <div class="row margin-tb-l"> <!-- внутри будут плитки филиалов -->

    <?php

    $args = array (
    'post_type'              => $post->post_type,
    'nopaging'               => false,
    'posts_per_page'         => '0',
    // 'order'                  => 'DESC',
    // 'orderby'                => 'ID',
);

$query = new WP_Query( $args );

if ( $query->have_posts() ) {
    while ( $query->have_posts() ) {
        $query->the_post();


        // if (get_the_post_thumbnail($post->ID)): // если есть картинка

              if ($post->post_type == 'mm_department'):
                  echo "<div class='column small-12 medium-6 large-4'>";
                elseif ($post->post_type == 'mm_branch'):
                  echo "<div class='column small-12 medium-6'>";
                elseif ($post->post_type == 'mm_employee'):
                  echo "<div class='column small-12 medium-4 large-3 padding-on-small'>";
                elseif ($post->post_type == 'mm_service'):
                  echo "<div class='column small-12 medium-4 large-3'>";
                else :
                  echo "<div class='column small-12 medium-4'>";
              endif; ?>

                  <a class="black-text" href="<?php the_permalink($post->ID); ?>">
                    <?php if ($post->post_type == 'mm_department'):
                     mm_thumbnail_with_placeholder_if_no_thumbnail("4x3-s", "full-width");
                    elseif ($post->post_type == 'mm_branch'):
                     mm_thumbnail_with_placeholder_if_no_thumbnail("16x9-s", "full-width");
                    elseif ($post->post_type == 'mm_employee'):
                      mm_thumbnail_with_placeholder_if_no_thumbnail("3x4-s", "full-width");
                    elseif ($post->post_type == 'mm_service'):
                      mm_thumbnail_with_placeholder_if_no_thumbnail("16x9-s", "full-width");
                    else :
                      mm_thumbnail_with_placeholder_if_no_thumbnail("1x1-s", "full-width");
                     endif ?>
                    <p class="text-center"><strong><?php the_title(); ?></strong></p>
                    <?php // the_excerpt(); ?>
                  </a>
                </div>

     <?php // endif; // конец условия "если есть картинка"

    }
} else {
    // display when no posts found
}

wp_reset_postdata();     // Restore original Post Data ?>

  </div>

  <?php } // это закрытие проверки условия наличия сотрудников в самом верху
  else {
    echo "<p>Список сотрудников пуст. Пожалуйста, заполните соответствующий раздел.</p>";
  }
  ?>

</div>

<?php
get_sidebar();
get_footer();
