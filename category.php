<!-- this is category.php -->

<?php get_header(); ?>

<div class="row column">

  <?php custom_breadcrumbs();
  if ($post) { // закрытие условия в самом низу

    if  ($post->post_type == 'mm_employee'): ?>
      <h1>Наши сотрудники</h1>
    <?php endif; ?>

  <div class="row margin-tb-l">



    <?php

    $args = array (
    'post_type'              => $post->post_type,
    'nopaging'               => false,
    'posts_per_page'         => '0',
    'cat'                  => $cat,
    // 'orderby'                => 'ID',
);

$query = new WP_Query( $args );

if ( $query->have_posts() ) {
    while ( $query->have_posts() ) {
        $query->the_post();



                  echo "<div class='column small-12 medium-4'>"; ?>

                  <a class="black-text" href="<?php the_permalink($post->ID); ?>">
                    <?php mm_thumbnail_with_placeholder_if_no_thumbnail("1x1-s", "full-width"); ?>
                    <p class="text-center"><strong><?php the_title(); ?></strong></p>
                    <?php // the_excerpt(); ?>
                  </a>
                </div>

     <?php // endif; // конец условия "если есть картинка"

    }
} else {
    // display when no posts found
}

wp_reset_postdata();     // Restore original Post Data ?>

  </div>

  <?php } // это закрытие проверки условия наличия сотрудников в самом верху
  else {
    echo "<p>Список сотрудников пуст. Пожалуйста, заполните соответствующий раздел.</p>";
  }
  ?>

</div>

<?php
get_sidebar();
get_footer();
