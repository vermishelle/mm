<?php

class Mm_attachment extends Mm_object {

  public $pod_name;

  public function __construct($id_parameter) {
    parent::__construct($id_parameter);
    $this->pod_name = 'Mm_attachment';
  }

  public function caption() {
    return get_post($this->id)->post_excerpt;
  }

  public function show($class = null, $size = null, $y_offset = "") {



    if ($this->mime() == "video/mp4"):
      $org = new Mm_org();
      // var_dump(wp_get_attachment_image($org->default_hero()->id));
      $str = '<video autoplay muted loop src="' . $this->url() . '" class="'. $class .'" poster="' . wp_get_attachment_url($org->default_hero()->id) . '"></video>';
      return $str;
    endif;

    if (empty($size)) {
      $size = 'full';
    }

    if (empty($y_offset)):
      $inline_style = "";
    else:
      $inline_style = "left: 50%; top: " . $y_offset . "%; transform: translate(-50%, -" . $y_offset . "%);";
    endif;



    return wp_get_attachment_image($this->id, $size, false, array("class"=>$class, "style"=>$inline_style));

  }

  public function url() {
    return wp_get_attachment_url( $this->id );
  }

  public function mime() {
    if (get_post($this->id)) return get_post($this->id)->post_mime_type;
  }
}
