<?php

class Mm_branch extends Mm_post {

  	public function __construct($id_parameter) {
	    parent::__construct($id_parameter);
      $this->pod_name = 'Mm_branch';
 	}

  public function main_slider_name() {
    return $this->pod()->field('main_slider_name');
  }

  public function is_a_root() {
  	$post_object = get_post( $this->id );
  	return ($post_object->post_parent) ? false : true;
  }

	public function root() {
		if (!$this->is_a_root()) {
			return new Mm_branch(get_post($this->id)->post_parent);
		} else {
			return NULL;
		}
	}

  public function children() {
    $args = array(
    	'post_parent' => $this->id,
    	// 'post_type'   => 'any',
    	'numberposts' => -1,
    	'post_status' => 'publish'
    );

    $children_wp_obj = get_children( $args );

    if (empty($children_wp_obj)) {
      return NULL;
    }

    $obj_arr = [];

    foreach ($children_wp_obj as $child) {
      array_push($obj_arr, new Mm_branch($child->ID));
    }

    usort($obj_arr, function($a, $b) {
        return $a->menu_order() <=> $b->menu_order();
    });

    return $obj_arr;
  }

  // Hierarchy end

  public function shema_proezda() {
    return $this->pod()->field('shema_proezda');
  }

  public function enquiry() {
    $phone_id = $this->pod()->field('enquiry')['ID'];
    return new Mm_phone($phone_id);
  }

  public function emergency() {
    $phone_id = $this->pod()->field('emergency')['ID'];
    return new Mm_phone($phone_id);
  }

  public function visit() {
    $phone_id = $this->pod()->field('visit')['ID'];
    return new Mm_phone($phone_id);
  }

  public function hotline() {
    $phone_id = $this->pod()->field('hotline')['ID'];
    return new Mm_phone($phone_id);
  }

  public function head() {
  	// return $this->pod()->field( 'mm_branch_head' )["ID"];

    if ($this->pod()->field( 'mm_branch_head' )) {
			return new Mm_employee($this->pod()->field( 'mm_branch_head' )["ID"]);
    } else {
    	return NULL;
    }
  }

  public function proezd() {
  	return $this->pod()->field('proezd');
  }

  public function working_time() {
  	return apply_filters('the_content', $this->pod()->field('working_time'));
  }

  public function adresses_covered() {
  	return apply_filters('the_content', $this->pod()->field('adresses_covered'));
  }

  public function phones() {
  	$arr = $this->pod()->field( 'phones' );

    if (empty($arr)) {
      return NULL;
    }

  	$obj_arr = [];
  	foreach ($arr as $arr_element) {
  		array_push($obj_arr, new Mm_phone($arr_element["ID"]));
  	};

    usort ($obj_arr, function ($a, $b) {return ($a->menu_order() - $b->menu_order()); });

    return $obj_arr;
  }

  public function administration() {
  	$arr = $this->pod()->field( 'administration' );

    if (empty($arr)) { return []; }

	$obj_arr = [];

  // если есть глава - сразу ставим первым

  if ($this->head()) { array_push($obj_arr, $this->head()); }

	foreach ($arr as $arr_element) {
    $empl = new Mm_employee($arr_element["ID"]);
    if ($this->head() && $empl == $this->head()) { continue; }
		array_push($obj_arr, $empl);
	};

  	usort ($obj_arr, function ($a, $b) {return ($a->menu_order() - $b->menu_order()); });

    return $obj_arr;
  }

  public function adress() {
  	return $this->pod()->field('adress');
  }


  public function departments() {
  	$arr = $this->pod()->field( 'mm_branch_departments' );
    if (empty($arr)) return null;

  	$obj_arr = [];
  	foreach ($arr as $arr_element) {
  		array_push($obj_arr, new Mm_department($arr_element["ID"]));
  	};

      usort($obj_arr, function($a, $b) {
          return $a->menu_order() <=> $b->menu_order();
      });

    	return $obj_arr;
  }

  public function some_phones_have_adress_or_schedule() {
    $contacts = $this->phones();

    foreach ($contacts as $contact) {
      if ($contact->room() or $contact->time()) {
        return true; // выходим и возвращем true как только наткнулись
      }
    }
    return false; // если не вышли с true раньше - выходим здесь с false
  }

  public function show_contacts_table() {

    $org = new Mm_org();

    if (empty($this->phones())):
      if ( sizeof($org->branches()) < 2 ):
        if (empty($org->phones())):
          return NULL;
        endif;
      endif;
    endif;

    if ( sizeof($org->branches()) < 2 ):
      $phones = $org->phones();
    else:
      $phones = $this->phones();
    endif;

    return show_contacts_table($this->phones());
  }

}
