<?php

class Mm_department extends Mm_post {

  public function __construct($id_parameter) {
    parent::__construct($id_parameter);
    $this->pod_name = 'Mm_department';
  }

  public function some_phones_have_adress_or_schedule() {
    $contacts = $this->phones();

    foreach ($contacts as $contact) {
      if ($contact->room() or $contact->time()) {
        return true; // выходим и возвращем true как только наткнулись
      }
    }
    return false; // если не вышли с true раньше - выходим здесь с false
  }


  public function head() {

    return $this->pod()->field('head') ? new Mm_employee($this->pod()->field('head')['ID']) : NULL;
  }

  public function employees() {
    $arr = $this->pod()->field( 'employees' );

    if (empty($arr)): return []; endif;

    $obj_arr = [];
    $head = $this->head();
    if ($head) { array_push($obj_arr, $head); }

    foreach ($arr as $arr_element) {
      $empl = new Mm_employee($arr_element["ID"]);
      if ($empl == $head) { continue; }
      array_push($obj_arr, $empl);
    };
    usort ($obj_arr, function ($a, $b) {return ($a->menu_order() - $b->menu_order()); });
    return $obj_arr;
  }

  public function hide_from_lists() {
    return $this->pod()->field('hide_from_lists');
  }

  public function branches() {

    $arr = $this->pod()->field( 'mm_department_branch' );

    if (empty($arr)) {
      return NULL;
    }

    $obj_arr = [];
    foreach ($arr as $arr_element) {
      array_push($obj_arr, new Mm_branch($arr_element["ID"]));
    };
    return $obj_arr;
  }

  public function services() {

    // если отделение корневое - отдаем его услуги как есть
    // если отделени дочерние:
    // - если в услугах очернего отделения пусто - отдаем родительские услуги
    // - если не пусто - отдаем как есть, услуги текущего дочернего отделения

    $current_dep_services = $this->pod()->field( 'services' );

    if ($this->is_a_root() or !empty($current_dep_services)):
      $arr = $current_dep_services;
    else:
      $root_id = wp_get_post_parent_id($this->id);
      $root_pod = pods('mm_department', $root_id);
      $arr = $root_pod->field('services');
    endif;

    // конец подсасывания родительских услуг

    if (empty($arr)) {
      return NULL;
    }

    $obj_arr = [];
    foreach ($arr as $arr_element) {
      $obj_element = new Mm_service($arr_element["ID"]);
      if (!$obj_element->hide_from_lists()):
        array_push($obj_arr, $obj_element);
      endif;
    };

    usort($obj_arr, function($a, $b) {
      return $a->menu_order() <=> $b->menu_order();
    });

    return $obj_arr;

  }

  // public function phones() {
  //
  //   $key = __METHOD__ . strval($this->id);
  //   $trans = get_transient( $key );
  //
  //   if (empty($trans)) {
  //     return set_transient($key, $this->phones_not_cached());
  //   }
  //   return $trans;
  // }

  public function phones() {

    $arr = $this->pod()->field( 'phones' );

    if (empty($arr)) {
      return NULL;
    }

    $obj_arr = [];
    foreach ($arr as $arr_element) {
      array_push($obj_arr, new Mm_phone($arr_element["ID"]));
    };
    return $obj_arr;

  }

  public function header_img_options() {
    return $this->pod()->field('header_img_options');
  }

  public function belongs_to_branch($branch) {
    if (empty($branch) || empty($this->branches())) {
      return false;
    }
    return in_array($branch, $this->branches());
  }

  public function is_a_root() {
    $post_object = get_post( $this->id );
    return ($post_object->post_parent) ? false : true;
  }

  public function root() {
    if (!$this->is_a_root()) {
      return new Mm_department(get_post($this->id)->post_parent);
    } else {
      return NULL;
    }
  }

  public function children() {
    $args = array(
      'post_parent' => $this->id,
      // 'post_type'   => 'any',
      'numberposts' => -1,
      'post_status' => 'publish'
    );

    $children_wp_obj = get_children( $args );

    if (empty($children_wp_obj)) {
      return NULL;
    }

    $obj_arr = [];

    foreach ($children_wp_obj as $child) {
      array_push($obj_arr, new Mm_department($child->ID));
    }

    usort($obj_arr, function($a, $b) {
      return $a->menu_order() <=> $b->menu_order();
    });

    return $obj_arr;
  }

}
