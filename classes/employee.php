<?php

class Mm_employee extends Mm_post {

  public function __construct($id_parameter) {
    parent::__construct($id_parameter);
    $this->pod_name = 'Mm_employee';
  }

  public function sertificate_number() {
    return $this->pod()->field('sertificate_number');
  }

  public function schedule() {
    return $this->pod()->field('schedule');
  }

  public function phones() {
    $arr = $this->pod()->field( 'phones' );
    $obj_arr = [];
    if (empty($arr)): return $obj_arr; endif;

    foreach ($arr as $arr_element) {
      array_push($obj_arr, new Mm_phone($arr_element["ID"]));
    };

    usort($obj_arr, function($a, $b) {
      return $a->menu_order() <=> $b->menu_order();
    });

    return $obj_arr;

  }

  public function resume() {
    return $this->pod()->field('resume');
  }

  public function position_type() {
    return $this->pod()->field('position_type');
  }

  public function position() {
    return $this->pod()->field('position');
  }

  public function spec_sertificate_educational_institution() {
    return $this->pod()->field('spec_sertificate_educational_institution');
  }

  public function spec_sertificate_date_received() {
    return $this->pod()->field('spec_sertificate_date_received');
  }

  public function spec_sertificate_expiration() {
    return $this->pod()->field('spec_sertificate_expiration');
  }

  public function spec_sertificate_speciality() {
    return $this->pod()->field('spec_sertificate_speciality');
  }


  public function category() {
    return $this->pod()->field('category');
  }

  public function category_specialty() {
    return $this->pod()->field('category_specialty');
  }

  public function category_assignment_date() {
    return $this->pod()->field('category_assignment_date');
  }

  public function departments() {
    return $this->pod()->field('departments');
  }

  public function branch() {
    return $this->pod()->field('branch');
  }

  public function head_of_department_in() {
    return $this->pod()->field('head_of_department_in');
  }

  public function show_card() {
    $mm_proportions = "3x4";
    $mm_class = "full-width";

    if( $this->has_thumbnail() ) { // $this->has_thumbnail()
      echo get_the_post_thumbnail($this->id, $mm_proportions, array( 'class' => $mm_class ));
    } else {
      mm_placeholder($mm_proportions, $mm_class);
    }
  }
}
