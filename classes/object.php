<?php

class Mm_object {

  public $id;

  public function __construct($id_parameter) {
    if (!is_numeric($id_parameter) and $id_parameter != null) die('id должен быть числом или null, а не ' . $id_parameter);
    $this->id = $id_parameter;
  }

  public function title() {
    return get_the_title($this->id);
  }

  public function menu_order() {
    $post_object = get_post( $this->id );
    return $post_object->menu_order;
  }

  public function post_type() {
    return get_post_type($this->id);
  }

}
