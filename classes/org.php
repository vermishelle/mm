<?php

class Mm_org {

  public $short_name;
  public $full_name;
  public $head;
  public $inn;
  public $ogrn;
  public $mail_address;
  public $license_text;
  public $license_scan;
  public $founder;
  public $payment_details;

  public function __construct() {
    $this->short_name = get_theme_mod('short_name');
    $this->full_name = get_theme_mod('full_name');
    if (get_theme_mod('head') != 0) $this->head = new Mm_employee(get_theme_mod('head'));
    $this->inn = get_theme_mod('inn');
    $this->ogrn = get_theme_mod('ogrn');
    $this->mail_address = get_theme_mod('mail_address');
    // $this->license_text = apply_filters('the_content', (get_theme_mod('license_text')));
    // не знаю, зачем я пропускал через фильтр, но после этого текст оказывается в тегах <p>
    // что мне мешает
    $this->license_text = (get_theme_mod('license_text'));
    if (get_theme_mod('license_scan') != 0) $this->license_scan = new Mm_attachment(get_theme_mod('license_scan'));
    $this->founder = get_theme_mod('founder');
    $this->payment_details = get_theme_mod('payment_details');
  }

  public function socials() {
    $res = [];
    $socials = pods('social', array ('limit' => -1));
    if (0 < $socials->total()) {
      while ($socials->fetch()) {
        $res[$socials->field('icon')] = $socials->field('link');
      }
    }
    return $res;
  }

  // исходим из того, что корневой филиал один.
  // если больше - получается хрень. но лимита в UI нет
  public function main_branch() {

    $args = array (
      'post_type'              => 'mm_branch',
      'nopaging'               => true,
      'posts_per_page'         => -1,
    );

    $it = NULL;

    $query = new WP_Query( $args );

    if ( $query->have_posts() ) {
      while ( $query->have_posts() ) {
        $query->the_post();
        if ((get_post(get_the_ID())->post_parent) == 0) { // is a root
          $it =  new Mm_branch(get_the_ID());
          break;
        } // end if
      } // end while
      wp_reset_postdata();
    } // end if

    return $it;
  }

  public function enquiry() {
    if ( $this->main_branch() == NULL) { return; }
    return $this->main_branch()->enquiry();
  }

  public function emergency() {
    if ( $this->main_branch() == NULL) { return; }
    return $this->main_branch()->emergency();
  }

  public function visit() {
    if ( $this->main_branch() == NULL) { return; }
    return $this->main_branch()->visit();
  }

  public function hotline() {
    if ( $this->main_branch() == NULL) { return; }
    return $this->main_branch()->hotline();
  }

  public function pricelist_has_oms_lkg() {
    $pl = $this->pricelist();

    foreach ($pl as $pl_el):
      if (!empty($pl_el->oms_lkg())): return true; endif;
    endforeach;

    return false;
  }

  public function services_without_departments(){

    $org_services = $this->services();

    $services_without_deps = [];

    if (empty($org_services)): return $services_without_deps; endif;

    foreach ($org_services as $org_service):
      if (empty($org_service->departments()) and !$org_service->hide_from_lists()):
        array_push($services_without_deps, new Mm_service($org_service->id));
      endif;
    endforeach;

    return $services_without_deps;
  }

  public function phones() {

    $arr = [];

    $args = array (
      'post_type'              => 'mm_phones',
      'nopaging'               => true,
      'posts_per_page'         => '0',
      // 'order'                  => 'DESC',
      // 'orderby'                => 'ID',
    );

    $query = new WP_Query( $args );

    if ( $query->have_posts() ) {
      while ( $query->have_posts() ) {
        $query->the_post();
        array_push($arr, new Mm_phone(get_the_ID()));
      } // end while

      wp_reset_postdata();

    } // end if

    return $arr;

  } // end function

  public function departments() {

    $obj_arr = [];

    $args = array (
      'post_type'              => 'mm_department',
      'nopaging'               => true,
      'posts_per_page'         => '0',
      // 'order'                  => 'DESC',
      'orderby'                => 'menu_order',
    );

    $query = new WP_Query( $args );

    if ( $query->have_posts() ) {
      while ( $query->have_posts() ) {
        $query->the_post();
        array_push($obj_arr, new Mm_department(get_the_ID()));
      } // end while

      wp_reset_postdata();

    } // end if
    usort($obj_arr, function($a, $b) {
      return $a->menu_order() - $b->menu_order();
    });

    return $obj_arr;

  } // end function

  public function employees() {

    $arr = [];

    $args = array (
      'post_type'              => 'mm_employee',
      'nopaging'               => true,
      'posts_per_page'         => '0',
      // 'order'                  => 'DESC',
      // 'orderby'                => 'ID',
    );

    $query = new WP_Query( $args );

    if ( $query->have_posts() ) {
      while ( $query->have_posts() ) {
        $query->the_post();
        $arr[] = new Mm_employee( get_the_ID() );
      } // end while
      wp_reset_postdata();
    } // end if

    return $arr;

  } // end function

  public function branches($limit = 0) {

    $obj_arr = [];

    $args = array ('post_type' => 'mm_branch', 'nopaging' => true,
    'posts_per_page'         => $limit,
    // 'order'                  => 'DESC',
    // 'orderby'                => 'ID',
  );

  $query = new WP_Query( $args );

  if ( $query->have_posts() ) {
    while ( $query->have_posts() ) {
      $query->the_post();
      array_push($obj_arr, new Mm_branch(get_the_ID()));
    } // end while

    wp_reset_postdata();

  } // end if

  return $obj_arr;
} // end function

public function pricelist($limit = 0) {

  $obj_arr = [];

  $args = array (
    'post_type'              => 'pricelist',
    'nopaging'               => true,
    'posts_per_page'         => 0,
    // 'order'                  => 'DESC',
    // 'orderby'                => 'menu_order',
  );

  $query = new WP_Query( $args );

  if ( $query->have_posts() ):
    while ( $query->have_posts() ):
      $query->the_post();
      array_push($obj_arr, new Mm_price(get_the_ID()));
    endwhile;
    wp_reset_postdata();
    return $obj_arr;
  endif;

}

public function services($limit = 0) {

  $obj_arr = [];

  if ($limit == 0):
    $args = array (
      'post_type'              => 'mm_service',
      'nopaging'               => true,
    );
  else:
    $args = array (
      'post_type'              => 'mm_service',
      'nopaging'               => false,
      'posts_per_page'         => $limit,
      'meta_key'   => 'hide_from_lists',
      'meta_value' => '0',
      // 'order'                  => 'DESC',
      // 'orderby'                => 'ID',
    );
  endif;

  $query = new WP_Query( $args );

  if ( $query->have_posts() ) {
    while ( $query->have_posts() ) {
      $query->the_post();
      array_push($obj_arr, new Mm_service(get_the_ID()));
    } // end while

    wp_reset_postdata();

  } // end if
  return $obj_arr;
} // end function

public function default_hero() {
  $global_header_id = get_theme_mod('header_image_data')->attachment_id;

  if (empty($global_header_id)) {
    return NULL;
  }
  $hero_obj = new Mm_attachment($global_header_id);
  return $hero_obj;

}

}
