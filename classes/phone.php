<?php

class Mm_phone extends Mm_post {

  public function __construct($id_parameter) {
    parent::__construct($id_parameter);
    $this->pod_name = 'Mm_phones';
  }

  public function phone_number() {
    return $this->pod()->field('phones');
  }

  public function branch() {
    return $this->pod()->field('branch');
  }
  public function department() {
    return $this->pod()->field('department');
  }

  public function service() {
    return $this->pod()->field('service');
  }

  public function employee() {
    return $this->pod()->field('employee');
  }

  public function room() {
    return $this->pod()->field('room');
  }

  public function time() {
    return $this->pod()->field('time');
  }

  public function note() {
    return $this->pod()->field('note');
  }

}
