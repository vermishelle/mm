<?php

class Mm_post extends Mm_object {

  public $pod_name; // вернуть protected

  public function __construct($id_parameter) {
      parent::__construct($id_parameter);
      $this->pod_name = $this->post_type();
  }

  public function pod() {
  	return pods($this->pod_name,  $this->id);
  }

  public function hero_or_fallback() {

    switch (true) {

      case !is_archive() and $this->has_hero():
        return $this->hero();
        break;

      case is_archive():
        $global_header_id = get_theme_mod('header_image_data')->attachment_id;
        $global_header_img_obj = new Mm_attachment($global_header_id);
        return $global_header_img_obj;
        break;

      // case $current_post->has_thumbnail():
      //   echo $current_post->thumbnail()->show($classes_for_hero);
      //   break;

      default:
        $global_header_id = get_theme_mod('header_image_data')->attachment_id;
        $global_header_img_obj = new Mm_attachment($global_header_id);
        return $global_header_img_obj;
        break;

    }

  }

  public function content($dummy_phrase = true) {

    $post_object = get_post( $this->id );
    $content = $post_object->post_content;

    if ($dummy_phrase == false and empty($content)) return '';

    switch ($this->post_type()) {
      case 'mm_service':
        $dummy_phrase = 'В поликлинике оказывается услуга &laquo;' . $this->title() . '&raquo;.';
        break;
      case 'mm_department':
        if (!empty($this->children())) {
          $dummy_phrase = 'В поликлинике работают &laquo;' . $this->title() . '&raquo;.';;
        } else {
          $dummy_phrase = 'В поликлинике работает &laquo;' . $this->title() . '&raquo;.';
        }
        break;
      case 'mm_branch':
        $dummy_phrase = 'В поликлинике работает &laquo;' . $this->title() . '&raquo;.';
        break;

      default:
        $dummy_phrase = 'Раздел заполняется.';
        break;
    }

    if (empty($content)) {
      return $dummy_phrase;
    } else {
      return apply_filters('the_content', $content);
    }
  }

  public function url() {
  	return get_permalink($this->id);
  }

	public function has_thumbnail() {
		return has_post_thumbnail($this->id);
	}

  public function has_hero() {
    if (empty(get_the_ID())):
      return false;
    endif;
		return !empty($this->pod()->field('hero'));
	}

  public function hero() {
    $hero_arr = $this->pod()->field('hero');
    $hero_obj = new Mm_attachment($hero_arr["ID"]);
    return $hero_obj;

  }

  public function hide_thumbnail() {
    $hide_thumbnail = pods($this->pod_name,  $this->id)->field('hide_thumbnail');
    if ('1' == $hide_thumbnail) {
      return true;
    } else {
      return false;
    }
  }

  public function gallery() {
    // фо факту pods заведены только для posts, pages, service

    // arr - то что указано в pods
    // arr_obj - объекты клвсса mm_attachment, первым из которых стоит
    // thumbnail, если она не svg

    $arr = $this->pod()->field( 'gallery' );

    if (empty($arr)): $arr = []; endif;

    $obj_arr = [];

    if ($this->has_thumbnail()):
      $not_svg = $this->thumbnail()->mime() != "image/svg+xml";
    else:
      $not_svg = false;
    endif;

    if ( $this->has_thumbnail() and !$this->hide_thumbnail() and $not_svg):
      array_push($obj_arr, $this->thumbnail());
    endif;

    if (!empty($arr)): // если есть чего добавить из pods
      foreach ($arr as $arr_element):
        array_push($obj_arr, new Mm_attachment($arr_element['ID']));
      endforeach;
    endif;

    return $obj_arr;
  }

  public function show_gallery() {

    $gallery = $this->gallery();
    if (empty($gallery)): return; endif;

    $first = true;

    // формат галлереи:
    // фото 3x4 вверху,
    // под ним 3x4  формата 3 иконки ?>

    <div class="row">
      <?php foreach ($gallery as $img):

        if ($first): ?>
          <div class="column small-12 lightbox-container">
            <a href="<?php echo $img->url() ?>"><?php echo $img->show('full-width thumbnail', '16x9') ?></a>
          </div>
          <?php $first = false;
          continue;
        endif; ?>

        <div class="column small-4 lightbox-container">
          <a href="<?php echo $img->url() ?>"><?php echo $img->show('full-width thumbnail', '16x9') ?></a>
        </div>
      <?php endforeach; ?>
    </div>


  <?php } // конец function show_gallery()

  public function thumbnail() {
    if (!$this->has_thumbnail()) {
      return new Mm_placeholder();
    }
    $thumbnail_id = get_post_thumbnail_id($this->id);
    return new Mm_attachment($thumbnail_id);
  }

  public function show_thumbnail($class = null, $size = null) {
    if ($this->has_thumbnail()) {
      return $this->thumbnail()->show($class, $size);
    } else {
      switch ($size) {
        case '1x1':
          $img = '<img class="' . $class . '" src="' . get_template_directory_uri() . '/img-dongles/placeholder-1x1.svg' . '" />';
          return $img;
        case '3x4':
          $img = '<img class="' . $class . '" src="' . get_template_directory_uri() . '/img-dongles/placeholder-3x4.svg' . '" />';
          return $img;
        case '4x3':
          $img = '<img class="' . $class . '" src="' . get_template_directory_uri() . '/img-dongles/placeholder-4x3.svg' . '" />';
          return $img;
        case '16x9':
          $img = '<img class="' . $class . '" src="' . get_template_directory_uri() . '/img-dongles/placeholder-16x9.svg' . '" />';
          return $img;

        default:
          # code...
          break;
      }
    }
  }

}
