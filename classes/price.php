<?php

class Mm_price extends Mm_post {

  public function __construct($id_parameter) {
      parent::__construct($id_parameter);
  }

  public function oms_lkg(){
    return $this->pod()->field('oms_lkg');
  }

  public function service() {
    $service = $this->pod()->field('mm_service');

    if (empty($service)) {
      return NULL;
    }

    return new Mm_service($service['ID']);
  }

  public function department() {

    $dep = $this->pod()->field('department');

    if (empty($dep)) {
      return NULL;
    }

    return new Mm_department($dep['ID']);
  }

  public function price() {
    return $this->pod()->field('mm_price');
  }

  public function article() {
    return $this->pod()->field('article');
  }

  public function subsection() {

    $key = __METHOD__ . strval($this->id);

    if ( false === ( $value = get_transient( $key ) ) ) {
      // this code runs when there is no valid transient set
      $fresh_result = $this->subsection_not_cached();
      set_transient($key, $fresh_result);
      return $fresh_result;
    }

    return $value;
  }

  public function subsection_not_cached() {

    $subsection = get_post_meta( $this->id, 'subsection', true );
    // echo xdebug_time_index(), "\n";

    $service = $this->service();

    if (empty($service)) {
      return $subsection;
    }

    if (empty($subsection)) {

      return $service->title();
    }

    return $subsection;

  }

// public function
}
