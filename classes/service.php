<?php

class Mm_service extends Mm_post {

  public function __construct($id_parameter) {
    parent::__construct($id_parameter);
    $this->pod_name = 'Mm_service';
  }

  public function departments() {
    return $this->pod()->field('departments');
  }

  public function phones() {
    $arr = $this->pod()->field( 'phones' );
    $obj_arr = [];
    foreach ($arr as $arr_element) {
      array_push($obj_arr, new Mm_phone($arr_element["ID"]));
    };

    usort($obj_arr, function($a, $b) {
      return $a->menu_order() <=> $b->menu_order();
    });

    return $obj_arr;

  }

  public function pricelist() {

    $key = __METHOD__ . strval($this->id);

    if ( false === ( $value = get_transient( $key ) ) ) {
      // this code runs when there is no valid transient set
      $fresh_result = $this->pricelist_not_cached();
      set_transient($key, $fresh_result);
      return $fresh_result;
    }

    return $value;
  }


  public function pricelist_not_cached() {
    $arr = $this->pod()->field( 'prices' );

    if (empty($arr)) {
      return [];
    }

    $obj_arr = [];
    foreach ($arr as $arr_element) {
      array_push($obj_arr, new Mm_price($arr_element["ID"]));
    };

    usort($obj_arr, function($a, $b) {
      return $a->menu_order() <=> $b->menu_order();
    });

    return $obj_arr;
  }

  public function hide_from_lists() {
    return $this->pod()->field('hide_from_lists');
  }

  public function header_img_options() {
    return $this->pod()->field('header_img_options');
  }

  public function has_phones() {
    return !empty($this->pod()->field('phones'));
  }

  public function some_phones_have_adress_or_schedule() {
    $contacts = $this->phones();

    foreach ($contacts as $contact) {
      if ($contact->room() or $contact->time()) {
        return true; // выходим и возвращем true как только наткнулись
      }
    }
    // если не вышли с true раньше - выходим здесь с false
    return false;
  }

  public function show_contacts_table() {
    // foreach ($this->phones() as $value) {
    //   echo $value->title();
    //   echo '<br>';
    //   # code...
    // }
    echo show_contacts_table($this->phones());
  }

  // public function subsections() {
  //
  //   $key = __METHOD__ . strval($this->id);
  //   $old_result = get_transient($key);
  //
  //   if ( !get_transient($key) ) {
  //      // this code runs when there is no valid transient set
  //      $fresh_result = $this->subsections_not_cached();
  //      set_transient($key, $fresh_result);
  //      return $fresh_result;
  //   }
  //
  //   return $value;
  // }

  private function pricelist_has_oms_lkg() {
    // return true;

    global $wpdb;

    // нереляционные связи хранятся в wp_postmeta, реляционные - в wp_podsrel
    // так, для справки, не совсем сюда относится

    $posts_table = $wpdb->posts;
    $meta_table = $wpdb->postmeta;
    $podsrel = $wpdb->prefix . "podsrel";

    $str = <<<HEREDOC
    SELECT oms_lkg.meta_value FROM $posts_table AS service
    JOIN $podsrel AS rel ON rel.item_id = service.ID
    JOIN $posts_table AS pricelist_positions ON pricelist_positions.ID = rel.related_item_id
    JOIN $meta_table AS oms_lkg ON pricelist_positions.ID = oms_lkg.post_id
    WHERE service.ID = $this->id AND oms_lkg.meta_key = 'oms_lkg' AND oms_lkg.meta_value IS NOT NULL AND oms_lkg.meta_value != ''
HEREDOC;

    $myrows = $wpdb->get_results( $str );

    if (empty($myrows)) {
      return false;
    } else {
      return true;
    }
  }

  private function subsections() {
    $pricelist = $this->pricelist();
    $subsections = [];

    if (empty($pricelist)) {
      return $subsections;
    }

    foreach ($pricelist as $pl_pos) {

      $current_subsection = $pl_pos->subsection();

      if (!in_array($current_subsection, $subsections)) {
        array_push($subsections, $current_subsection);
      }
    }

    return $subsections;
  }

  public function pricelist_of_subsection($subsection_arg) {
    $pl_obj_arr = [];

    $pricelist = $this->pricelist();

    foreach ($pricelist as $pl_pos) {
      $current_subsection = $pl_pos->subsection();
      if ($current_subsection == $subsection_arg) {
        array_push($pl_obj_arr, $pl_pos);
      }
    }

    return $pl_obj_arr;

  }

  public function show_pricelist_table() {



    $subsections = $this->subsections();

    // var_dump($subsections);

    foreach ($subsections as $subsection):

      // вывели название подсекции если она не одна
      // или одна, но не совпадает с названим услуги

      if (count($this->subsections()) > 1 or  $this->subsections() == 1 && $this->subsections()[0] != $this->title()) { ?>
        <h3><?php echo $subsection ?></h3>
        <?php } // конец проверки выводить ли название подсекции ?>


        <table class="pricelist-table">
          <thead>
            <tr>

              <?php if (!get_theme_mod('hide_article')): ?>
                <th class="show-for-medium article">
                  Артикул
                </th>
              <?php endif; ?>
              <th>
                Наименование
              </th>
              <th class="price">
                Цена, &#8381;
              </th>
              <?php
              $org = new Mm_org();
              $has_oms_lkg = $this->pricelist_has_oms_lkg();
              if ($has_oms_lkg): ?>
              <th class="oms_lkg">
                ОМС/ЛКГ&#42;
              </th>
            <?php endif; ?>
          </tr>
        </thead>
        <?php foreach ($this->pricelist_of_subsection($subsection) as $pp_position) { ?>
          <tr>
            <?php if (!get_theme_mod('hide_article')): ?>
              <td class="show-for-medium"><?php echo $pp_position->article() ?></td>
            <?php endif; ?>
            <td><?php echo $pp_position->title() ?></td>
            <td ><?php echo $pp_position->price() ?></td>
            <?php if ($has_oms_lkg): ?>
              <td><?php
              if (empty($pp_position->oms_lkg())): echo "—";
            else: echo $pp_position->oms_lkg();
          endif;
          ?></td>
        <?php endif; ?>
      </tr>
      <?php } // внутри подсекции перебрали позиции прайслиста ?>
    </table>

  <?php endforeach; // конец перебора подсекций

  if ($has_oms_lkg) echo "<small>ОМС - бесплатно по полису обязательного медцинского страхования. ЛКГ - бесплатно для льготных категорий граждан.</small>";

}

public function free_service() {
  if ($this->pod()->field('free_service') == '1') {return true;};
  return false;
}

}
