<?php

// !!! вы можете оставить отзыв об услуге... и тп
// через кастомайзер отрулить

if (!comments_open()) return;

$feedback_types_arr = ['mm_service', 'mm_employee', 'mm_branch', 'mm_department'];
$comments_mode = null;

if (is_single(['Задать вопрос главному врачу', 'glavnyj-vrach']) or is_page('Задать вопрос главному врачу')):
  $comments_mode = 'question';
elseif (is_page(['Отзывы', 'Обратная связь']) or is_singular($feedback_types_arr) or is_post_type_archive($feedback_types_arr)):
  $comments_mode = 'feedback';
endif;

 $disqus_site_id = get_theme_mod('disqus_site_id');

// DISQUS or native WP comments
if (empty($disqus_site_id)):

  // в свиче рассматриваем следующие случаи:
  // вопросы - для главврача
  // отзывы - для врачей, услуг, отделений и филиалов, как singular так и архив



  switch ($comments_mode):
    case 'question':
    $form_options = [
      'title_reply' => 'Задайте ваш вопрос',
      'label_submit' => 'Отправить вопрос',
      'class_submit' => 'button submit',
      'comment_notes_before' => '<p class="callout warning small">Вопрос будет опубликован после проверки (с целью защиты от спама). Ваши адрес электронной почты и телефон не будут опубликованы.</p>'
    ];
      break;
    case 'feedback':
      $form_options = [
        'title_reply' => 'Оставьте отзыв',
        'label_submit' => 'Отправить отзыв',
        'class_submit' => 'button submit',
        'comment_notes_before' => '<p class="callout warning small">Отзыв будет опубликован после проверки (с целью защиты от спама). Ваши адрес электронной почты и телефон не будут опубликованы.</p>'
      ];
      break;
    default:
      $form_options = [
        'title_reply' => 'Оставьте комментарий',
        'label_submit' => 'Отправить комментарий',
        'class_submit' => 'button submit',
        'comment_notes_before' => '<p class="callout warning small">Сообщение будет опубликовано после проверки (с целью защиты от спама). Ваши адрес электронной почты и телефон не будут опубликованы.</p>'
      ];
      $comments = get_approved_comments(mm_get_the_ID());
  endswitch; ?>

  <section class='row column padding-top'>

    <?php if (!empty($comments)):

      if ($comments_mode == 'question'): ?>
        <h2>Вопросы главному врачу</h2>
      <?php elseif ($comments_mode == 'feedback'): ?>
        <h2>Отзывы</h2>
      <?php else: ?>
        <h2>Комментарии</h2>
      <?php endif;

      wp_list_comments(null, $comments);

    endif; ?>
    <div class='padding-top'>
      <?php comment_form($form_options); ?>
    </div>

  </section>

<?php else: // пошла история с DISQUS ?>

  <div id="disqus_thread"></div>
  <?php global $post;
  $postid = $post->ID;
  $permalink = get_permalink($postid); ?>
  <script>
      /**
       *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
       *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables
       */

      var disqus_config = function () {
          this.page.url = '<?php echo $permalink ?>';  // Replace PAGE_URL with your page's canonical URL variable
          this.page.identifier = '<?php echo $postid ?>'; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
      };

      (function() {  // DON'T EDIT BELOW THIS LINE
          var d = document, s = d.createElement('script');

          s.src = '//<?php echo $disqus_site_id ?>.disqus.com/embed.js';

          s.setAttribute('data-timestamp', +new Date());
          (d.head || d.body).appendChild(s);
      })();
  </script>
  <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>

<?php endif;
