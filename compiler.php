<?php
require_once( get_template_directory() . '/vendor/autoload.php');
use Pug\Pug;

return new Pug(array(
    'prettyprint' => true,
    'extension' => '.pug',
    'cache' => get_template_directory() . '/cache/pug/'
));
