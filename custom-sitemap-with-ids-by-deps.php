<?php
/**
 * Template Name: Услуги с id (для прайслиста)
 */
?>

<?php get_header(); ?>

<div class="row column margin-b-l">

  <h2>Услуги по отделениям</h2>
  <?php $org = new Mm_org();

  $deps = $org->departments();

  foreach ($deps as $dep): ?>

    <h3><?php echo $dep->title(), ' / id: ', $dep->id, ' / menu order: ', $dep->menu_order(); ?></h3>

    <?php $services = $dep->services();

    if (empty($services)): continue; endif; ?>

    <ul>
      <?php foreach ($services as $service): ?>
        <li>
          <?php echo $service->title(), ' / id: ', $service->id, ' / menu order: ', $service->menu_order();  ?>
        </li>
      <?php endforeach; ?>
    </ul>

  <?php endforeach; ?>

  <?php if(!empty($org->services_without_departments())): ?>

    <h2>Услуги, не привязанные к отделениям</h2>

    <ul>
      <?php foreach ($org->services_without_departments() as $serv_wo_dep): ?>
          <li>
            <?php echo $serv_wo_dep->title(), ' (id: ', $serv_wo_dep->id, ' )', $serv_wo_dep->menu_order();  ?>
          </li>
      <?php endforeach; ?>
    </ul>

  <?php endif; ?>
</div>

<?php get_footer();
