<?php
/**
 * Template Name: Custom sitemap page with ids
 */
?>

<?php get_header(); ?>
<?php get_template_part( 'template-parts/all', 'hero_banner' ); ?>
  <div class="row">
  	<div class="column small-12">
   	 <?php custom_breadcrumbs(); ?>
    </div>
  </div>
<div class="row column margin-b-l">

	<?php mm_show_posttype_for_sitemap_with_ids("page"); ?>
	<?php mm_show_posttype_for_sitemap_with_ids("post"); ?>
	<?php mm_show_posttype_for_sitemap_with_ids("mm_branch"); ?>
	<?php mm_show_posttype_for_sitemap_with_ids("mm_department"); ?>
	<?php mm_show_posttype_for_sitemap_with_ids("mm_service"); ?>
	<?php mm_show_posttype_for_sitemap_with_ids("mm_employee"); ?>

</div>

<?php get_footer();
