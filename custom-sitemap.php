<?php
/**
 * Template Name: Custom sitemap page
 */
?>

<?php get_header(); ?>
<?php get_template_part( 'template-parts/all', 'hero_banner' ); ?>
  <div class="row">
  	<div class="column small-12">
   	 <?php custom_breadcrumbs(); ?>
    </div>
  </div>
<div class="row column margin-b-l">

	<?php mm_show_posttype_for_sitemap("page"); ?>
	<?php mm_show_posttype_for_sitemap("post"); ?>
	<?php mm_show_posttype_for_sitemap("mm_branch"); ?>
	<?php mm_show_posttype_for_sitemap("mm_department"); ?>
	<?php mm_show_posttype_for_sitemap("mm_service"); ?>
	<?php mm_show_posttype_for_sitemap("mm_employee"); ?>

</div>

<?php get_footer();

