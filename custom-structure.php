<?php
/**
 * Template Name: Custom org structure page
 */
?>

<?php get_header(); ?>
<?php get_template_part( 'template-parts/all', 'hero_banner' ); ?>
  <div class="row">
  	<div class="column small-12">
   	 <?php custom_breadcrumbs(); ?>
    </div>
  </div>
<div class="row column margin-b-l">

	<?php mm_show_posttype_for_sitemap("mm_branch"); ?>
	<?php mm_show_posttype_for_sitemap("mm_department"); ?>

</div>

<?php get_footer();

