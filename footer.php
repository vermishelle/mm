  <?php get_template_part( 'template-parts/footer', 'main' ); ?>
  <?php wp_footer(); ?>

  <?php if (!is_user_logged_in()):
    echo get_theme_mod('code_snippets');
  endif; ?>
  </body>
  <!-- <?php echo get_num_queries(); ?> queries in <?php timer_stop(1); ?> seconds. -->
  <?php time_from_start(); ?>
</html>
