<!-- this is front-page.php -->
<?php
  get_header();
  // get_template_part( 'template-parts/home', 'video' );
?>

<!-- <div class="move-up"> -->
<?php get_template_part( 'template-parts/all', 'hero_banner' ); ?>
  <?php get_template_part( 'template-parts/home', 'welcome_blocks' ); ?>
  <?php get_template_part( 'template-parts/home', 'content' ); ?>
  <div class="skew-separator--top__complimentary-dark"></div>
  <?php get_template_part( 'template-parts/home', 'departments' ); ?>
  <div class="skew-separator--bottom__complimentary-dark"></div>

  <?php get_template_part( 'template-parts/home', 'services' ); ?>

  <div class="home-offer-separator--top"></div>

  <?php get_template_part( 'template-parts/home', 'offer' ); ?>

  <div class="home-offer-separator--bottom"></div>

  <?php get_template_part( 'template-parts/home', 'news' ); ?>

  <?php get_template_part( 'template-parts/home', 'authorities' ); ?>

<!-- </div>  -->
<!-- .move-up -->

<?php // comments_template(); ?>

<?php get_sidebar(); ?>
<?php get_footer(); ?>
