<?php
$start_time = microtime(true);
require_once 'helpers/code_helpers.php';

require_once 'classes/object.php';
require_once 'classes/post.php';
require_once 'classes/attachment.php';
require_once 'classes/branch.php';
require_once 'classes/department.php';
require_once 'classes/employee.php';
require_once 'classes/org.php';
require_once 'classes/phone.php';
require_once 'classes/placeholder.php';
require_once 'classes/price.php';
require_once 'classes/service.php';
require_once 'classes/widgets.php';

require_once 'helpers/unsorted.php';

require_once('inc/filters.php');
require_once('inc/actions.php');
require_once('inc/service_functions.php');
require_once('inc/theme_functions.php');
include_once('inc/breadcrumbs.php');
require_once('inc/cpt-declaration.php');
require_once('inc/custom-thumbnails.php');
include_once('inc/should-be-plugin-shortcodes.php');
require_once('inc/enqueue.php');
require_once('inc/theme-support.php');
require_once('inc/menu.php');

// include_once('inc/test.php');
require_once('inc/snippets.php');
require_once('inc/sitemap.php');
require_once('inc/customizer.php');
// require_once('inc/plugin_pricelist_import.php');

require_once 'helpers/view_helpers.php';
