var gulp             = require('gulp');
var sass             = require('gulp-sass');
var plumberNotifier  = require('gulp-plumber-notifier');
var concat           = require('gulp-concat');
var postcss      = require('gulp-postcss');
var sourcemaps   = require('gulp-sourcemaps');
var autoprefixer = require('autoprefixer');
var concat = require('gulp-concat');
var minify = require('gulp-minify');

var src = {
    scss: './stylesheets/scss/**/*.scss',
    fonts: './bower_components/font-awesome/fonts/*.{eot,svg,ttf,woff,woff2,otf}',
    js: ['./bower_components/js-cookie/src/js.cookie.js', './js/src/init.magnific-popup.js', './bower_components/magnific-popup/dist/jquery.magnific-popup.min.js', './js/src/app.js']
};



// было в сырцах js еще 'bower_components/foundation-sites/dist/foundation.min.js' и 'bower_components/jquery/dist/jquery.min.js'


var dest = {
    js: './js',
    css: './stylesheets/css',
    fonts: './stylesheets/fonts'
}

gulp.task('default', ['serve']);

gulp.task('serve', ['sass', 'copy'], function() {
    gulp.watch(src.scss, ['sass']);
    gulp.watch(src.js, ['javascript']);
});

// Компилирую SCSS (включая foundation на своих переменных)
gulp.task('sass', function() {
    return gulp.src(src.scss)
        .pipe(plumberNotifier())
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'compressed', includePaths: ['./bower_components/foundation-sites/scss/']}))
        .pipe(postcss([ autoprefixer({ browsers: ['last 2 versions', 'ie >= 9', 'and_chr >= 2.3'] }) ]))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(dest.css));
});

gulp.task('copy', ['copy-fonts', 'javascript', 'copy-css']);

// Copy fonts from font Awesome
gulp.task('copy-fonts', function() {

  // копируем шрифты
  return gulp.src(src.fonts)
      .pipe(gulp.dest(dest.fonts));

});

gulp.task('javascript', function() {
  return gulp.src(src.js)
    .pipe(sourcemaps.init())
    .pipe(concat('all.js'))
    // .pipe(sourcemaps.write())
    .pipe(minify())
    .pipe(gulp.dest(dest.js));
});

gulp.task('copy-css', function() {

  // копируем font awesome css
  return gulp.src(['./bower_components/font-awesome/css/font-awesome.min.css', './bower_components/magnific-popup/dist/magnific-popup.css'])
      .pipe(gulp.dest(dest.css));

});
