<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?> lang="ru-RU">
  <head>
    <meta charset="UTF-8">
    <title><?php wp_title(); ?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:400,700|Scada&amp;subset=latin,cyrillic" type="text/css" media="all">

    <!-- wp_head start -->
    <?php wp_head(); ?>
    <!-- wp_head end -->
  </head>
  <body <?php body_class(); ?>>

    <?php echo get_theme_mod('code_snippets'); ?>
    <?php get_template_part( 'template-parts/menu', 'top' ); ?>
<!--  -->
