<?php

// output helpers


function time_from_start() {
  // there must be $start_time var set in header.php
  global $start_time;
  $time = microtime(true) - $start_time;
  echo "<!-- Rendering time:" . $time . '-->';
}

function mm_print($title, $content, $title_tag = 'h3', $content_tag = 'p', $test_var = "undefined for sure") {

  if(empty($content)) return;
  if($test_var == "undefined for sure") $test_var = $content;

  if (!empty($test_var)):

    if (!empty($title_tag)):
      $title_opening_tag = "<$title_tag>";
      $title_closing_tag = "</$title_tag>";
    else:
      $title_opening_tag = '';
      $title_closing_tag = '';
    endif;

    if (!empty($content_tag)):
      $content_opening_tag = "<$content_tag>";
      $content_closing_tag = "</$content_tag>";
    else:
      $content_opening_tag = '';
      $content_closing_tag = '';
    endif;

    echo $title_opening_tag . $title . $title_closing_tag;
    echo $content_opening_tag . $content . $content_closing_tag;
  endif;
}

function mm_show_ya_map($adress, $title) { ?>
  <div id="map" style="width: 100%; height: 40vmin" class='margin-b'></div>
  <script>
    ymaps.ready(init);
    var myMap;

    function init(){
      mm_adress = '<?php echo $adress ?>';
      mm_title = '<?php echo $title ?>';
      var myGeocoder = ymaps.geocode(mm_adress, {results: 1});
      myGeocoder.then(
          function (res) {
              myMap = new ymaps.Map("map", {
                  center: res.geoObjects.get(0).geometry.getCoordinates(),
                  zoom: 14,
                    controls: ["zoomControl"]
              });
              myMap.behaviors.disable('scrollZoom');
            result = res.geoObjects.get(0);
            result.properties.set('iconContent', mm_title);
            result.options.set('preset', 'islands#darkBlueStretchyIcon');
              myMap.geoObjects.add(result);
          },
          function (err) {
              // обработка ошибки
          }
      );
    }
  </script>
<?php }

function is_ext_url($url) {
  if (strpos($url, 'http') !== false) {
    return true;
}
}

// ООП helpers

function show_contacts_table($phones) {

  if (empty($phones)): return NULL; endif;
  ob_start(); ?>
    <table class="table-cells-proportions">
      <?php foreach ($phones as $phone): ?>
        <tr>
          <td>
            <p><?php echo $phone->title() ?></p>
          </td>

          <td>
            <p><?php echo $phone->phone_number() ?><p>
            <p>
              <?php if ($phone->room()): ?>
                Кабинет №<?php echo $phone->room() ?><br>
              <?php endif; ?>

              <?php if ($phone->time()): ?>
                Режим работы: <?php echo $phone->time() ?>
              <?php endif; ?>
            </p>
          </td>
        </tr>
      <?php endforeach; ?>
    </table>
  <?php $out = ob_get_contents();
  ob_end_clean();
  return $out;
}

function mm_show_services_in_circles($services) { ?>
  <div class="row padding-tb-l">
    <?php foreach ($services as $service):
      if ($service->hide_from_lists()) {
        continue;
      } ?>
      <div class='column small-6 medium-3 large-2'>
        <?php
        // картинка смотрится хорошо с шириной 100%,
        // а иконка - с шириной 70%
        //этому и посвящен сей workaround

        if ($service->has_thumbnail()) {

          if ($service->thumbnail()->mime() == "image/svg+xml") {
            $cl = "almost-full-width";
          } else {
            $cl = "full-width";
          }
    }?>
        <a class="black-text" href="<?php echo $service->url(); ?>">
          <!-- задача -
          1) квадратные контейнеры
          2)  -->
          <div class="fixed-proportions-box hover-effect">    <!-- margin-b -->
            <div class="fixed-proportions-content center-vertically bg-white circle">
                <?php echo $service->show_thumbnail("<?php echo $cl ?> no-more-than-container", "1x1") ?>
            </div>
          </div>
          <p class="text-center padding-tb-s"><strong><?php echo $service->title() ?></strong></p>
        </a>
      </div>
    <?php endforeach; ?>
  </div>
<?php }

function mm_department_has_commercial_services($department_id) {

    $pod = pods( 'mm_department',  $department_id);
    $services = $pod->field( 'services' );

    if ($services) { // если есть хоть какие услуги - начинаем их перебирать и должны по идее если я правильно понимаю php выскочить из цикла и вернуть true при первой найденой услуге без галки "прятать"
      foreach ($services as $service) {
        if (!get_post_meta($service['ID'])["hide_from_lists"][0]) { // если галка "прятать" не стоит
          return true;
        } // конец проверки на галку
      } // конец перебора услуг с целью найти хоть одную не спрятанную
    }

    return false; // если перебрав услуги не нашли не спрятанной - дошли сюда (иначе бы вышли) и возвращаем false

}

function mm_thumbnail_with_placeholder_if_no_thumbnail($mm_proportions, $mm_class) {
  if( has_post_thumbnail() ) {
    the_post_thumbnail($mm_proportions, array( 'class' => $mm_class ));
  } else {
    mm_placeholder($mm_proportions, $mm_class);
  }
}

function mm_show_thumbnail_with_placeholder_if_empty($ID, $mm_proportions, $mm_class) {
  if( has_post_thumbnail($ID) ) {
    echo get_the_post_thumbnail($ID, $mm_proportions, array( 'class' => $mm_class ));
  } else {
    mm_placeholder($mm_proportions, $mm_class);
  }
}

function mm_lorem($mm_size) {
  if ($mm_size == "small") {
    echo "<p>Данный текст является примером и присутствует здесь только потому, что необходимый контент не готов, а некоторое количество текста все равно необходимо для правильного восприятия дизайна. Этот текст скоро будет удален, как только будет предоставлен необходимый контент.<br>Если вы видите этот текст на работающем сайте, пожалуйста, напишите администратору сайта, поскольку такого быть не должно.</p>"; }
  elseif ($mm_size == "big") {
    echo "<p>Данный текст является примером и присутствует здесь только потому, что необходимый контент не готов, а некоторое количество текста все равно необходимо для правильного восприятия дизайна. Этот текст скоро будет удален, как только будет предоставлен необходимый контент.</p>

    <p>Если вы видите этот текст на работающем сайте, пожалуйста, напишите администратору сайта, поскольку такого быть не должно. Данный текст является примером и присутствует здесь только потому, что необходимый контент не готов, а некоторое количество текста все равно необходимо для правильного восприятия дизайна. Этот текст скоро будет удален, как только будет предоставлен необходимый контент.</p>

    <p>Если вы видите этот текст на работающем сайте, пожалуйста, напишите администратору сайта, поскольку такого быть не должно.</p>";}
}

function mm_content_with_placeholder() {

global $post;

  if (get_the_content()) {
    the_content();
  } elseif ($post->post_type == "mm_service") { ?>
    <p>В поликлинике оказывается услуга "<?php the_title() ?>".</p>
  <?php }
  elseif ($post->post_type == "mm_department") { ?>
    <p>В поликлинике работает <?php the_title() ?>.</p>
  <?php }
  elseif ($post->post_type == 'mm_branch') { ?>
    <p>В поликлинике работает <?php the_title() ?>.</p>
  <?php }
  else {
    // echo file_get_contents('http://loripsum.net/api/3/small');
    mm_lorem("big");
  }
}

// более суровый вариант - вместо dummy-текста просто шиш, ничего не показываем
function mm_details_about() {
    if (get_the_content()) { ?>
      <h2>Подробнее</h2>
    <?php the_content();
  }
}

function mm_show_child_departments_info_if_any($post_ID) {

  if( mm_department_has_children($post_ID) ) { // у поста есть дети
    ?><h2>Список отделений данного типа</h2><?php
    $children = get_pages(array('child_of' => $post_ID, 'post_type' => 'mm_department'));
    foreach ($children as $child) { ?>
      <p><a href="<?php echo get_permalink($child); ?>"><?php echo $child->post_title ?> (<?php mm_show_list_of_department_branches($child->ID); ?>)</a></p>
    <?php }
  }
}

function mm_department_has_children($post_ID) {
  $children = get_pages(array('child_of' => $post_ID, 'post_type' => 'mm_department'));
  return count( $children ) != 0;
}

function mm_show_list_of_department_branches($department_id) {
  $pod = pods( 'mm_department', $department_id );
  $department_branches = $pod->field( 'mm_department_branch' );

  // делаем список вида "филиал1, филиал2"
  mm_show_list_of_titles_from_arrays($department_branches);
}

function mm_show_list_of_titles_from_arrays($mm_arrays) {

  if (!$mm_arrays) {
    return; // если массив пустой - ничего не печатаем
  }

  $i = 0;
  $len = count($mm_arrays);
  $string_of_titles = "";

  foreach ($mm_arrays as $mm_array) {
    $string_of_titles = $string_of_titles . ($mm_array["post_title"]);
    if ($i < $len - 1) {
      $string_of_titles = $string_of_titles . ", ";
    }
    $i++;
  }
  echo $string_of_titles;
}

function mm_show_the_title() {
    is_archive() ? post_type_archive_title() : the_title();
}
