<?php
function show_widget_area($widget_area_id) {
  if (is_active_sidebar($widget_area_id)) {
    dynamic_sidebar($widget_area_id);
  }
}

function social_icons() {
  $org = new Mm_org();
  $socials = $org->socials();
  $res = '';
  foreach ($socials as $icon => $link) {
    $res .= "<a href=\"$link\" target=\"_blank\"><i class=\"fa fa-$icon\" aria-hidden=\"true\"></i></a>\n";
  }
  return $res;
}

function authority_icons() {
  // образцово-показательная выборка из базы в духе Pods

  $result = '';

    $authorities = pods('authority', array ('limit' => -1));
    if (0 < $authorities->total()) {
      while ($authorities->fetch()) {
        $id = $authorities->field('id');
        $icon_img = mm_get_card($id); //<img...
        $title = get_the_title($id); // pure string
        $link = $authorities->field('link'); // string http://...

        $result .= <<<HEREDOC
<div class="column small-12 medium-6 large-4">
  <a href="$link" target="_blank">
  <div class="row">
    <div class="column small-3">
      $icon_img
    </div>
    <div class="column small-9">
      <p class='font-color-accent-darkest'>$title</p>
    </div>
  </div>
  </a>
</div>
HEREDOC;
      }
    }
    return $result;
}

function mm_get_placeholder($proportions, $class) {
  // отрезаем у $proportions хвосты -s, -l, xl и тд.
  $proportions = preg_replace('/(.*)-.*/', '$1', $proportions);
  if (!in_array($proportions, ['1x1', '3x4', '4x3', '16x9'])) { return nil; }
  $url = get_template_directory_uri() . '/img-dongles/placeholder-' . $proportions . '.svg';
  return "<img class='$class' src='$url'>";
}

function mm_placeholder($proportions, $class) {
  echo mm_get_placeholder($proportions, $class);
}

function mm_get_card($id, $proportions = "3x4-s", $class = "full-width") {
  if( has_post_thumbnail($id) ) { // $this->has_thumbnail()
    return get_the_post_thumbnail($id, $proportions, array( 'class' => $class ));
  } else {
    return mm_get_placeholder($proportions, $class);
  }
}

function mm_show_card($id, $proportions = "3x4-s", $class = "full-width") {
  echo mm_get_card($id, $proportions, $class);
}

function mm_get_schedule_cards($employees) {
  if (!is_array($employees)) { die('$employees is not an array'); };
  $output = '<div class="row">';
  foreach ($employees as $empl) {
    $title = $empl->title();
    $position = $empl->position();
    $url = $empl->url();
    $schedule=apply_filters( 'the_content', $empl->schedule());

    $phones = $empl->phones();



    $output .= "<div class='column small-4 padding-on-small margin-b'>";
    $output .= "<div class='bg-white padding-s'>";
    $output .= "<a class='black-text' href='$url'>";
    $output .= mm_get_card($empl->id);
    $output .= '</a>';
    $output .= '</div>';
    $output .= '</div>';
    $output .= "<div class='column small-8 padding-on-small margin-b'>";
    $output .= "<p class=''><strong>$title</strong><br>$position<br><br>$schedule</p>";
    if(!empty($phones)) {
    // $output .= '<h3>Контактная информация</h3>';
    $output .= show_contacts_table($phones);
    }
    $output .= '</div>';

  };
  $output .= '</div>';
  return $output;
}

function mm_get_cards($employees) {
  if (!is_array($employees)) { die('$employees is not an array'); };
  $output = '<div class="row">';
  foreach ($employees as $empl) {
    $title = $empl->title();
    $position = $empl->position();
    $url = $empl->url();

    $output .= "<div class='column small-12 medium-4 large-3 padding-on-small margin-b'>";
    $output .= "<div class='bg-white padding-s'>";
    $output .= "<a class='black-text' href='$url'>";
    $output .= mm_get_card($empl->id);
    $output .= "<p class='text-center'><strong>$title</strong><br>$position</p>";
    $output .= '</a>';
    $output .= '</div>';
    $output .= '</div>';
  };
  $output .= '</div>';
  return $output;
}

function mm_show_cards($employees) {
  echo mm_get_cards($employees);
}

function mm_get_dep_doctors($dep) {
  $dep_doctors = $dep->employees();
  if (empty($dep_doctors)) { return; };
  $title = $dep->title();
  $url = $dep->url();
  $out .= "<div class='row column padding-top-l'><h3><a href='$url'>$title</a></h3></div>";
  $out .= mm_get_cards($dep_doctors);
  return $out;
}

function mm_show_dep_doctors($dep) {
  echo  mm_get_dep_doctors($dep);
}

function mm_get_branch_doctors($branch) {
  $deps = $branch->departments();
  $title = $branch->title();
  $out = "<div class='row column padding-top-l'><h2>Врачи $title</h2></div>";
  foreach ($deps as $dep) {
    $out .= mm_get_dep_doctors($dep);
  }
  return $out;
}

function mm_show_branch_doctors($branch) {
  echo mm_get_branch_doctors($branch);
}

function mm_get_branch_admins($branch) {
  $admins = $branch->administration();
  if (empty($admins)) { return; };
  $title = $branch->title();
  $out .= "<div class='row column padding-top-l'><h2>Администрация $title</h2></div>";
  $out .= mm_get_cards($admins);
  return $out;
}

function mm_get_branch_admins_schedule($branch) {
  $admins = $branch->administration();
  if (empty($admins)) { return; };
  $title = $branch->title();
  $out .= "<div class='row column padding-top-l'><h2>Администрация $title</h2></div>";
  $out .= mm_get_schedule_cards($admins);
  return $out;
}

function mm_show_branch_admins($branch) {
  echo mm_get_branch_admins($branch);
}

function mm_show_branch_admins_schedule($branch) {
  echo mm_get_branch_admins_schedule($branch);
}
