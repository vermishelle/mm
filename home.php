<!-- this is home.php -->
<!-- здесь список блог-постов -->
<?php get_header(); ?>

<?php // get_template_part( 'template-parts/all', 'hero_banner' ); ?>

<!-- выводим тело страницы -->

<div class="row column">
  <?php custom_breadcrumbs(); ?>

  <?php if (have_posts()) :
     while (have_posts()) :
        the_post(); ?>
          <small><?php the_date(); ?></small>
          <h2><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h2>
          <div class="padding-b">
          <?php the_excerpt(); ?>
          </div>
     <?php endwhile;
  endif; ?>
</div>



<?php
get_sidebar();
get_footer();
