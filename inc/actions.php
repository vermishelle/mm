<?php

function mm_register_widget_areas() {
  register_sidebar( array(
    'name'          => 'Footer',
    'id'            => 'footer',
    'description'   => 'Под нижним меню',
    'before_widget' => '<div class="column">',
    'after_widget'  => '</div>',
    'before_title'  => '<h2>',
    'after_title'   => '</h2>',
  ) );
}

add_action( 'widgets_init', function() {
  register_widget( 'Foo_Widget' );
  mm_register_widget_areas();
});

add_action('mm_show_org', function($org){

  // пришлось громоздить чтоб не вызывать метод на null когда head пустой
  if (!empty($org->head)):
    $head_str = $org->head->title();
  else:
    $head_str = null;
  endif;

  // женим лицензию со сканом лицензии

  if (!empty($org->license_scan)):
    $license_str = sprintf('%1$s Вы можете посмотреть лицензию в формате <a href="%2$s" target="_blank">%3$s</a>.', $org->license_text, $org->license_scan->url(), $org->license_scan->mime());
  else:
    $license_str = $org->license_text;
  endif;


  $arr = [
    [$org->short_name, 'Краткое наименование'],
    [$org->full_name, ' Полное наименование'],
    [$head_str, 'Главный врач'],
    [$org->inn, 'ИНН'],
    [$org->ogrn, 'ОГРН'],
    [$org->mail_address, 'Почтовый адрес'],
    [$license_str, 'Лицензия'],
    [$org->founder, 'Учредитель'],
    [$org->payment_details, 'Платежные реквизиты']
  ];

  // проверяем, вдруг все пусто и затевать секцию не нужно
  $notemty = false;
  foreach ($arr as $item):
    if (!empty($item[0])) $not_empty = true;
  endforeach;

  // если все значения пустые - выходим
  if (!$not_empty) return; ?>

  <section class='row column'>
    <h2>Реквизиты организации</h2>
    <?php foreach ($arr as $item): ?>
      <?php if (empty($item[0])) { continue; } ?>
      <p><b><?php echo $item[1] ?></b>: <?php echo $item[0] ?></p>
    <?php endforeach; ?>
  </section>

  <?php });
