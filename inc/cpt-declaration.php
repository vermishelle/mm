<?php

// это могло бы быть плагином, если бы тема не была жестко заточена на использование CPT
// если перейти с использования дополнительных полей CPT в php-шаблонах на шорткоды, будет иметь
// смысл вынести это в плагин. В текущем виде - не имеет смысла, пусть будет так.

add_action( 'init', 'mm_my_cpt_init' );


// renaming posts to news from http://revelationconcept.com/wordpress-rename-default-posts-news-something-else/

function revcon_change_post_label() {
    global $menu;
    global $submenu;
    $menu[5][0] = 'Новости';
    $submenu['edit.php'][5][0] = 'Новости';
    $submenu['edit.php'][10][0] = 'Добавить Новости';
    $submenu['edit.php'][16][0] = 'Метки Новостей';
    echo '';
}
function revcon_change_post_object() {
    global $wp_post_types;
    $labels = &$wp_post_types['post']->labels;
    $labels->name = 'Новости';
    $labels->singular_name = 'Новость';
    $labels->add_new = 'Добавить Новости';
    $labels->add_new_item = 'Добавить Новость';
    $labels->edit_item = 'Редактировать Новость';
    $labels->new_item = 'Новость';
    $labels->view_item = 'Посмотреть Новость';
    $labels->search_items = 'Искать в Новостях';
    $labels->not_found = 'Новости не найдены';
    $labels->not_found_in_trash = 'Новостей в корзине не найдено';
    $labels->all_items = 'Все Новости';
    $labels->menu_name = 'Новости';
    $labels->name_admin_bar = 'Новости';
}

add_action( 'admin_menu', 'revcon_change_post_label' );
add_action( 'init', 'revcon_change_post_object' );

// end renaming posts to news


function mm_my_cpt_init() {

  register_post_type( 'mm_branch',
    array(
      'labels' => array(
        'name' => __( 'Организация' ),
        'singular_name' => __( 'Подразделение' )
      ),
      'public' => true,
      'has_archive' => true,
      'menu_icon' => 'dashicons-admin-multisite',
      'rewrite' => array( 'slug' => 'organization' ),
      'supports' => array( 'title', 'editor', 'thumbnail', 'comments', 'page-attributes' ),
      'hierarchical' => true,
    )
  );

  register_post_type( 'mm_department',
    array(
      'labels' => array(
        'name' => __( 'Отделения' ),
        'singular_name' => __( 'Отделение' )
      ),
      'public' => true,
      'has_archive' => true,
      'menu_icon' => 'dashicons-networking',
      'rewrite' => array( 'slug' => 'otdeleniya' ),
      'supports' => array( 'title', 'editor', 'thumbnail', 'comments', 'page-attributes' ),
      'capability_type'     => 'post',
      'hierarchical' => true,
    )
  );

  register_post_type( 'mm_employee',
    array(
      'labels' => array(
        'name' => __( 'Сотрудники' ),
        'singular_name' => __( 'Сотрудник' )
      ),
      'public' => true,
      'has_archive' => true,
      'menu_icon' => 'dashicons-id',
      'rewrite' => array( 'slug' => 'sotrudniki' ),
      'supports' => array( 'title', 'thumbnail', 'comments' ),
    )
  );

  register_post_type( 'mm_service',
    array(
      'labels' => array(
        'name' => __( 'Услуги' ),
        'singular_name' => __( 'Услуга' )
      ),
      'public' => true,
      'has_archive' => true,
      'menu_icon' => 'dashicons-plus-alt',
      'rewrite' => array( 'slug' => 'uslugi' ),
      'supports' => array( 'title', 'editor', 'thumbnail', 'comments', 'page-attributes'),
    )
  );

  register_post_type( 'mm_phones',
    array(
      'labels' => array(
        'name' => __( 'Телефоны' ),
        'singular_name' => __( 'Телефон' )
      ),
      'public' => false,
      'show_ui' => true,
      // 'has_archive' => true,
      'menu_icon' => 'dashicons-phone',
      'rewrite' => array( 'slug' => 'telefony' ),
      'supports' => array('title'),
    )
  );

}

function my_rewrite_flush() {
    mm_my_cpt_init();
    flush_rewrite_rules();
}
add_action( 'after_switch_theme', 'my_rewrite_flush' );
