<?php
function custom_thumbs()
{
  add_image_size( '1x1', 200, 200, true );
  add_image_size( '1x1-s', 400, 400, true );
  add_image_size( '1x1-m', 600, 600, true );
  add_image_size( '1x1-l', 800, 800, true );
  add_image_size( '1x1-xl', 1200, 1200, true );
  add_image_size( '1x1-xxl', 1800, 1800, true );

  add_image_size( '3x4', 198, 264, true );
  add_image_size( '3x4-s', 399, 532, true );
  add_image_size( '3x4-m', 600, 800, true );
  add_image_size( '3x4-l', 798, 1064, true );
  add_image_size( '3x4-xl', 1200, 1600, true );
  add_image_size( '3x4-xxl', 1800, 2400, true );

  add_image_size( '16x9', 352, 198, true );
  add_image_size( '16x9-s', 704, 396, true );
  add_image_size( '16x9-m', 1056, 594, true );
  add_image_size( '16x9-l', 1408, 792, true );
  add_image_size( '16x9-xl', 2128, 1197, true );
  add_image_size( '16x9-xxl', 3200, 1800, true );

  add_image_size( '4x3', 264, 198, true );
  add_image_size( '4x3-s', 532, 399, true );
  add_image_size( '4x3-m', 800, 600, true );
  add_image_size( '4x3-l', 1064, 798, true );
  add_image_size( '4x3-xl', 1600, 1200, true );
  add_image_size( '4x3-xxl', 2400, 1800, true );

  add_image_size( 'proportional', 200, 200, false );
  add_image_size( 'proportional-s', 400, 400, false );
  add_image_size( 'proportional-m', 600, 600, false );
  // add_image_size( 'proportional-l', 800, 800, false ); это дефолтный medium
  // добавится еще 1024 - это дефолтный large
  add_image_size( 'proportional-xl', 1200, 1200, false );
  add_image_size( 'proportional-xxl', 1800, 1800, false );
}
add_action( 'after_setup_theme', 'custom_thumbs' );
