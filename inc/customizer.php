<?php

function mm_customize_register($wp_customize) {

  // Информация об организации

  $wp_customize->add_section("org", array(
    "title" => 'Об организации',
    "priority" => 10,
  ));

  $wp_customize->add_setting("short_name", array(
    "default" => "",
    "transport" => "refresh",
  ));

  $wp_customize->add_control(new WP_Customize_Control(
    $wp_customize,
    "short_name",
    array(
      "label" => 'Краткое наименование организации',
      "section" => "org",
      "settings" => "short_name",
      "type" => "text",
    )
  ));

  $wp_customize->add_setting("full_name", array(
    "default" => "",
    "transport" => "refresh",
  ));

  $wp_customize->add_control(new WP_Customize_Control(
    $wp_customize,
    "full_name",
    array(
      "label" => 'Полное наименование организации',
      "section" => "org",
      "settings" => "full_name",
      "type" => "textarea",
    )
  ));

  $wp_customize->add_setting("head", array(
    "default" => "",
    "transport" => "refresh",
  ));


    function mm_get_employees_choices() {
      $org = new Mm_org();
      $obj_arr = $org->employees();
      $id_and_title = array();
      $id_and_title[NULL] = '–– Select ––';
      foreach ($obj_arr as $obj) {
        $name = $obj->title();
        $position = $obj->position();
        if (empty($position)):
          $str = $name;
        else:
          $str = sprintf('%1$s (%2$s)', $name, $position);
        endif;
        $id_and_title[$obj->id] = $str;
      }
      return $id_and_title;
    }

  $wp_customize->add_control(new WP_Customize_Control(
    $wp_customize,
    "head",
    array(
      "label" => 'Руководитель организации',
      "section" => "org", // Site Title & Tagline core section
      "settings" => "head",
      "type" => "select",
      'choices' => mm_get_employees_choices(),
    )
  ));

  $wp_customize->add_setting("inn", array(
    "default" => "",
    "transport" => "refresh",
  ));

  $wp_customize->add_control(new WP_Customize_Control(
    $wp_customize,
    "inn",
    array(
      "label" => 'ИНН',
      "section" => "org",
      "settings" => "inn",
      "type" => "text",
    )
  ));

  $wp_customize->add_setting("ogrn", array(
    "default" => "",
    "transport" => "refresh",
  ));

  $wp_customize->add_control(new WP_Customize_Control(
    $wp_customize,
    "ogrn",
    array(
      "label" => 'ОГРН и дата регистрации',
      "section" => "org",
      "settings" => "ogrn",
      "type" => "textarea",
    )
  ));

  $wp_customize->add_setting("mail_address", array(
    "default" => "",
    "transport" => "refresh",
  ));

  $wp_customize->add_control(new WP_Customize_Control(
    $wp_customize,
    "mail_address",
    array(
      "label" => 'Почтовый адрес',
      "section" => "org",
      "settings" => "mail_address",
      "type" => "text",
    )
  ));


  $wp_customize->add_setting("license_text", array(
    "default" => "",
    "transport" => "refresh",
  ));

  $wp_customize->add_control(new WP_Customize_Control(
    $wp_customize,
    "license_text",
    array(
      "label" => 'Лицензия',
      'description' => 'Номер, кем выдана, когда выдана, срок действия',
      "section" => "org",
      "settings" => "license_text",
      "type" => "textarea",
    )
  ));

  $wp_customize->add_setting("license_scan", array(
    "default" => "",
    "transport" => "refresh",
  ));

  $wp_customize->add_control(new WP_Customize_Media_Control(
    $wp_customize,
    "license_scan",
    array(
      "label" => 'Скан лицензии',
      "section" => "org",
      "settings" => "license_scan",
    )
  ));

  $wp_customize->add_setting("founder", array(
    "default" => "",
    "transport" => "refresh",
  ));

  $wp_customize->add_control(new WP_Customize_Control(
    $wp_customize,
    "founder",
    array(
      "label" => 'Сведения об учредителе',
      "section" => "org",
      "settings" => "founder",
      "type" => "textarea",
    )
  ));

  $wp_customize->add_setting("payment_details", array(
    "default" => "",
    "transport" => "refresh",
  ));

  $wp_customize->add_control(new WP_Customize_Control(
    $wp_customize,
    "payment_details",
    array(
      "label" => 'Платежные реквизиты',
      "section" => "org",
      "settings" => "payment_details",
      "type" => "textarea",
    )
  ));

  // Цвета - использую стандартную секцию colors

  $wp_customize->add_setting("color_scheme", array(
    "default" => "",
    "transport" => "refresh",
  ));

  $wp_customize->add_control(new WP_Customize_Control(
    $wp_customize,
    "color_scheme",
    array(
      "label" => 'Цветовая схема',
      "section" => "colors", // Site Title & Tagline core section
      "settings" => "color_scheme",
      "type" => "radio",
      'choices'  => array(
  			'blue'  => 'blue',
  			'green_light' => 'green light',
        'green_dark' => 'green dark',
  		),

    )
  ));

  // Сотрудники

  $wp_customize->add_section("employees", array(
    "title" => 'Сотрудники',
    // "priority" => 30,
  ));

  $wp_customize->add_setting("employees_display_on_archive_page_mode", array(
    "default" => true,
    "transport" => "refresh",
  ));

  $wp_customize->add_setting("empl_full_list_page", array(
    "default" => "",
    "transport" => "refresh",
  ));

  $wp_customize->add_control(new WP_Customize_Control(
    $wp_customize,
    "employees_display_on_archive_page_mode",
    array(
      "label"    => 'Что показывать в списке сотрудников:',
      "section"  => "employees",
      "settings" => "employees_display_on_archive_page_mode",
      'type'     => "select",
      'choices'  => ['admins_only'   => 'Только администрация',
                     'doctors_only' => 'Только врачи',
                     'all'          => 'Администрация и врачи'],
    )
  ));

  $wp_customize->add_control(new WP_Customize_Control(
    $wp_customize,
    "empl_full_list_page",
    array(
      "label" => 'Страница с полным списком сотрудников',
      'description' => 'Выберите страницу, на которой размещен общий список сотрудников в виде таблицы',
      "section" => "employees", // Site Title & Tagline core section
      "settings" => "empl_full_list_page",
      "type" => "dropdown-pages",
    )
  ));

  // Представление списка услуг - с разбивкой по отделениям или нет

  $wp_customize->add_section("services_list", array(
    "title" => 'Список услуг',
    'active_callback' => function () { return is_post_type_archive('mm_service'); },
    // "priority" => 30,
  ));

  $wp_customize->add_setting("split_by_departments", array(
    "default" => "",
    "transport" => "refresh",
  ));

  $wp_customize->add_control(new WP_Customize_Control(
    $wp_customize,
    "split_by_departments",
    array(
      "label" => 'Делить услуги на отделения?',
      "section" => "services_list", // Site Title & Tagline core section
      "settings" => "split_by_departments",
      "type" => "checkbox",
    )
  ));

  // Вакансии

  $wp_customize->add_section("vacancies", array(
		"title" => 'Вакансии',
    'active_callback' => function () { return is_post_type_archive('vacancies'); },
		// "priority" => 30,
	));

  $wp_customize->add_setting("vacancies_text", array(
    "default" => "",
    "transport" => "refresh",
  ));

  $wp_customize->add_control(new WP_Customize_Control(
    $wp_customize,
    "vacancies_text",
    array(
      "label" => 'Текст на странице списка вакансий',
      "section" => "vacancies", // Site Title & Tagline core section
      "settings" => "vacancies_text",
      "type" => "textarea",
    )
  ));

  $wp_customize->add_setting("hr", array(
    "default" => "",
    "transport" => "refresh",
  ));


  $wp_customize->add_control(new WP_Customize_Control(
    $wp_customize,
    "hr",
    array(
      "label" => 'Начальник отдела кадров',
      "section" => "vacancies", // Site Title & Tagline core section
      "settings" => "hr",
      "type" => "select",
      'choices' => mm_get_employees_choices(),
    )
  ));

  // Logo
  //
  // $wp_customize->add_setting("mm_logo", array(
  //   "default" => "",
  //   "transport" => "refresh",
  // ));

  $wp_customize->add_setting("logo_dark_on_light", array(
    "default" => "",
    "transport" => "refresh",
  ));

  $wp_customize->add_control(new WP_Customize_Media_Control(
    $wp_customize,
    "logo_dark_on_light",
    array(
      "label" => 'Логотип темный',
      "description" => 'Для размещения на светлом фоне',
      "section" => "title_tagline", // Site Title & Tagline core section
      "settings" => "logo_dark_on_light",
    )
  ));



  $wp_customize->add_setting("logo_light_on_dark", array(
    "default" => "",
    "transport" => "refresh",
  ));

  $wp_customize->add_control(new WP_Customize_Media_Control(
    $wp_customize,
    "logo_light_on_dark",
    array(
      "label" => 'Логотип светлый',
      "description" => 'Для размещения на темном фоне',
      "section" => "title_tagline", // Site Title & Tagline core section
      "settings" => "logo_light_on_dark",
    )
  ));



  //// front page

  $wp_customize->add_section("front_page", array(
		"title" => 'Главная страница',
		"priority" => 30,
    'active_callback' => function () { return is_front_page(); },
	));

  // weclome blocks

  // блок 1
  $wp_customize->add_setting("block1_title", array(
    "default" => "",
    "transport" => "refresh",
  ));

  $wp_customize->add_control(new WP_Customize_Control(
    $wp_customize,
    "block1_title",
    array(
      "label" => 'Блок1 название',
      "section" => "front_page",
      "settings" => "block1_title",
      "type" => "text",
    )
  ));

  $wp_customize->add_setting("block1_desc", array(
    "default" => "",
    "transport" => "refresh",
  ));

  $wp_customize->add_control(new WP_Customize_Control(
    $wp_customize,
    "block1_desc",
    array(
      "label" => 'Блок1 подпись',
      "section" => "front_page",
      "settings" => "block1_desc",
      "type" => "textarea",
    )
  ));

  $wp_customize->add_setting("block1_icon", array(
    "default" => "",
    "transport" => "refresh",
  ));

  $wp_customize->add_control(new WP_Customize_Control(
    $wp_customize,
    "block1_icon",
    array(
      "label" => 'Блок1 пиктограмма',
      'description' => 'Код иконки из библиотеки font awesome со страницы http://fontawesome.io/icons/ ',
      "section" => "front_page",
      "settings" => "block1_icon",
      "type" => "text",
    )
  ));

  $wp_customize->add_setting("block1_url", array(
    "default" => "",
    "transport" => "refresh",
  ));

  $wp_customize->add_control(new WP_Customize_Control(
    $wp_customize,
    "block1_url",
    array(
      "label" => 'Блок1 ссылка',
      'description' => 'Относительная ссылка - вида /path/to/something/',
      "section" => "front_page",
      "settings" => "block1_url",
      "type" => "text",
    )
  ));

  // блок 2
  $wp_customize->add_setting("block2_title", array(
    "default" => "",
    "transport" => "refresh",
  ));

  $wp_customize->add_control(new WP_Customize_Control(
    $wp_customize,
    "block2_title",
    array(
      "label" => 'Блок2 название',
      "section" => "front_page",
      "settings" => "block2_title",
      "type" => "text",
    )
  ));

  $wp_customize->add_setting("block2_desc", array(
    "default" => "",
    "transport" => "refresh",
  ));

  $wp_customize->add_control(new WP_Customize_Control(
    $wp_customize,
    "block2_desc",
    array(
      "label" => 'Блок2 подпись',
      "section" => "front_page",
      "settings" => "block2_desc",
      "type" => "textarea",
    )
  ));

  $wp_customize->add_setting("block2_icon", array(
    "default" => "",
    "transport" => "refresh",
  ));

  $wp_customize->add_control(new WP_Customize_Control(
    $wp_customize,
    "block2_icon",
    array(
      "label" => 'Блок2 пиктограмма',
      'description' => 'Код иконки из библиотеки font awesome со страницы http://fontawesome.io/icons/ ',
      "section" => "front_page",
      "settings" => "block2_icon",
      "type" => "text",
    )
  ));

  $wp_customize->add_setting("block2_url", array(
    "default" => "",
    "transport" => "refresh",
  ));

  $wp_customize->add_control(new WP_Customize_Control(
    $wp_customize,
    "block2_url",
    array(
      "label" => 'Блок2 ссылка',
      'description' => 'Относительная ссылка - вида /path/to/something/',
      "section" => "front_page",
      "settings" => "block2_url",
      "type" => "text",
    )
  ));

  // блок 3
  $wp_customize->add_setting("block3_title", array(
    "default" => "",
    "transport" => "refresh",
  ));

  $wp_customize->add_control(new WP_Customize_Control(
    $wp_customize,
    "block3_title",
    array(
      "label" => 'Блок3 название',
      "section" => "front_page",
      "settings" => "block3_title",
      "type" => "text",
    )
  ));

  $wp_customize->add_setting("block3_desc", array(
    "default" => "",
    "transport" => "refresh",
  ));

  $wp_customize->add_control(new WP_Customize_Control(
    $wp_customize,
    "block3_desc",
    array(
      "label" => 'Блок3 подпись',
      "section" => "front_page",
      "settings" => "block3_desc",
      "type" => "textarea",
    )
  ));

  $wp_customize->add_setting("block3_icon", array(
    "default" => "",
    "transport" => "refresh",
  ));

  $wp_customize->add_control(new WP_Customize_Control(
    $wp_customize,
    "block3_icon",
    array(
      "label" => 'Блок3 пиктограмма',
      'description' => 'Код иконки из библиотеки font awesome со страницы http://fontawesome.io/icons/ ',
      "section" => "front_page",
      "settings" => "block3_icon",
      "type" => "text",
    )
  ));

  $wp_customize->add_setting("block3_url", array(
    "default" => "",
    "transport" => "refresh",
  ));

  $wp_customize->add_control(new WP_Customize_Control(
    $wp_customize,
    "block3_url",
    array(
      "label" => 'Блок3 ссылка',
      'description' => 'Относительная ссылка - вида /path/to/something/',
      "section" => "front_page",
      "settings" => "block3_url",
      "type" => "text",
    )
  ));

  // блок 4
  $wp_customize->add_setting("block4_title", array(
    "default" => "",
    "transport" => "refresh",
  ));

  $wp_customize->add_control(new WP_Customize_Control(
    $wp_customize,
    "block4_title",
    array(
      "label" => 'Блок4 название',
      "section" => "front_page",
      "settings" => "block4_title",
      "type" => "text",
    )
  ));

  $wp_customize->add_setting("block4_desc", array(
    "default" => "",
    "transport" => "refresh",
  ));

  $wp_customize->add_control(new WP_Customize_Control(
    $wp_customize,
    "block4_desc",
    array(
      "label" => 'Блок4 подпись',
      "section" => "front_page",
      "settings" => "block4_desc",
      "type" => "textarea",
    )
  ));

  $wp_customize->add_setting("block4_icon", array(
    "default" => "",
    "transport" => "refresh",
  ));

  $wp_customize->add_control(new WP_Customize_Control(
    $wp_customize,
    "block4_icon",
    array(
      "label" => 'Блок4 пиктограмма',
      'description' => 'Код иконки из библиотеки font awesome со страницы http://fontawesome.io/icons/ ',
      "section" => "front_page",
      "settings" => "block4_icon",
      "type" => "text",
    )
  ));

  $wp_customize->add_setting("block4_url", array(
    "default" => "",
    "transport" => "refresh",
  ));

  $wp_customize->add_control(new WP_Customize_Control(
    $wp_customize,
    "block4_url",
    array(
      "label" => 'Блок4 ссылка',
      'description' => 'Относительная ссылка - вида /path/to/something/',
      "section" => "front_page",
      "settings" => "block4_url",
      "type" => "text",
    )
  ));

  // текст к отделениям

  $wp_customize->add_setting("about_departments", array(
    "default" => "",
    "transport" => "refresh",
  ));

  $wp_customize->add_control(new WP_Customize_Control(
    $wp_customize,
    "about_departments",
    array(
      "label" => 'Текст об отделениях на главной',
      "section" => "front_page",
      "settings" => "about_departments",
      "type" => "textarea",
    )
  ));

  $wp_customize->add_setting("about_services", array(
    "default" => "",
    "transport" => "refresh",
  ));

  // текст к услугам

  $wp_customize->add_control(new WP_Customize_Control(
    $wp_customize,
    "about_services",
    array(
      "label" => 'Текст об услугах на главной',
      "section" => "front_page",
      "settings" => "about_services",
      "type" => "textarea",
    )
  ));

  // про спецпредложение

  $wp_customize->add_setting("home_cta_title", array(
    "default" => "",
    "transport" => "refresh",
  ));

  $wp_customize->add_control(new WP_Customize_Control(
    $wp_customize,
    "home_cta_title",
    array(
      "label" => 'Заголовок спецпредложения',
      "section" => "front_page",
      "settings" => "home_cta_title",
      "type" => "text",
    )
  ));

  $wp_customize->add_setting("home_cta_desc", array(
    "default" => "",
    "transport" => "refresh",
  ));

  $wp_customize->add_control(new WP_Customize_Control(
    $wp_customize,
    "home_cta_desc",
    array(
      "label" => 'Описание спецпредложения',
      "section" => "front_page",
      "settings" => "home_cta_desc",
      "type" => "textarea",
    )
  ));

  $wp_customize->add_setting("home_cta_service", array(
    "default" => "",
    "transport" => "refresh",
  ));

  function mm_get_services_choices() {
    $org = new Mm_org();
    $services_obj_arr = $org->services();
    $srv_id_and_title = array();
    foreach ($services_obj_arr as $services_obj) {
      $srv_id_and_title[$services_obj->id] = $services_obj->title();
    }
    return $srv_id_and_title;
  }

  $wp_customize->add_control(new WP_Customize_Control(
    $wp_customize,
    "home_cta_service",
    array(
      "label" => 'Услуга, продвигаемая в спецпредложении',
      "section" => "front_page",
      "settings" => "home_cta_service",
      "type" => "select",
      'choices' => mm_get_services_choices(),
    )
  ));

  $wp_customize->add_setting("home_cta_button_text", array(
    "default" => "",
    "transport" => "refresh",
  ));

  $wp_customize->add_control(new WP_Customize_Control(
    $wp_customize,
    "home_cta_button_text",
    array(
      "label" => 'Текст кнопки',
      'description' => 'Текст на кнопке, ведущей к спецпредложению',
      "section" => "front_page",
      "settings" => "home_cta_button_text",
      "type" => "text",
    )
  ));

  $wp_customize->add_setting("home_cta_background_img", array(
    "default" => "",
    "transport" => "refresh",
  ));

  $wp_customize->add_control(new WP_Customize_Media_Control(
    // Image_Control возвращает url вместо id, поэтому лучше Media
    $wp_customize,
    "home_cta_background_img",
    array(
      "label" => 'Фоновое изображение для блока спецпредложения',
      'description' => "Если не задано, будет использован thumbnail услуги",
      "section" => "front_page",
      "settings" => "home_cta_background_img",
    )
  ));

  // offer bg image offset

  $wp_customize->add_setting("home_offer_bg_img_y_offset", array(
    "default" => "50",
    "transport" => "refresh",
  ));

  $wp_customize->add_control(new WP_Customize_Control(
    $wp_customize,
    "home_offer_bg_img_y_offset",
    array(
      "label" => 'Смещение фоновой картинки спецпредложения на главной по оси Y от 0 до 100',
      'description' => '0 - верхний край ближе к центру, 50 - середина картинки к центру, 100 - низ картинки ближе к центру',
      "section" => "front_page", // Site Title & Tagline core section
      "settings" => "home_offer_bg_img_y_offset",
      "type" => "number",
    )
  ));

  // codes

	$wp_customize->add_section("codes", array(
		"title" => __("Коды", "musweb-medical"),
		// "priority" => 0,
	));

  $wp_customize->add_setting("code_snippets", array(
    "default" => "",
    "transport" => "refresh",
  ));

  $wp_customize->add_control(new WP_Customize_Control(
    $wp_customize,
    "code_snippets",
    array(
      "label" => __("Коды счетчиков", "musweb-medical"),
      "section" => "codes",
      "settings" => "code_snippets",
      "type" => "textarea",
    )
  ));

  $wp_customize->add_setting("disqus_site_id", array(
    "default" => "",
    "transport" => "refresh",
  ));

  $wp_customize->add_control(new WP_Customize_Control(
    $wp_customize,
    "disqus_site_id",
    array(
      "label" => __("Disqus \"site id\" code", "musweb-medical"),
      'description' => 'Его можно создать или найти в личном кабинете на сайте Disqus.com: в административной панели на вкладке General, позиция Shortname.<br><br>Очистите это поле чтобы воспользоваться стандартными комментариями WordPress.',
      "section" => "codes",
      "settings" => "disqus_site_id",
      "type" => "text",
    )
  ));

  // галка спрятать артикул
  $wp_customize->add_section("pricelist", array(
    "title" => __("Прайс-лист", "musweb-medical"),
    // "priority" => 0,
  ));

  $wp_customize->add_setting("pricelist_page", array(
    "default" => "",
    "transport" => "refresh",
  ));

  $wp_customize->add_control(new WP_Customize_Control(
    $wp_customize,
    "pricelist_page",
    array(
      "label" => __("URL страницы с прайслистом (полный адрес вида http://site.com/url.html или http://site.com/url.pdf)", "musweb-medical"),
      "section" => "pricelist",
      "settings" => "pricelist_page",
      "type" => "text",
    )
  ));

  $wp_customize->add_setting("hide_article", array(
    "default" => false,
    "transport" => "refresh",
  ));


  $wp_customize->add_control(new WP_Customize_Control(
    $wp_customize,
    "hide_article",
    array(
      "label" => 'Не показывать артикул в прайс-листе? (для внедленного в страницы услуг прайслиста)',
      "section" => "pricelist",
      "settings" => "hide_article",
      "type" => "checkbox",
    )
  ));


}

add_action("customize_register","mm_customize_register");
