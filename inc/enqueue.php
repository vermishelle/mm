<?php
function google_fonts() {
  $query_args = array(
    'family' => 'Open+Sans:400,700',
    'subset' => 'cyrillic,latin',
  );
  wp_register_style( 'google_fonts', add_query_arg( $query_args, "//fonts.googleapis.com/css" ), array(), null );
}

add_action('wp_enqueue_scripts', 'google_fonts');

function mm_scripts_and_styles() {

  mm_enqueue_local_script('main-js', '/js/all-min.js', array('jquery'), true);
  mm_enqueue_local_style('font-awesome', '/stylesheets/css/font-awesome.min.css');

  if (get_theme_mod('color_scheme') == 'green_light'):
    mm_enqueue_local_style('main-style', '/stylesheets/css/app_green_light.css');
    elseif (get_theme_mod('color_scheme') == 'green_dark'):
      mm_enqueue_local_style('main-style', '/stylesheets/css/app_green_dark.css');
    else:
      mm_enqueue_local_style('main-style', '/stylesheets/css/app.css');
    endif;

    // wp_enqueue_script( 'dzm-counter', 'https://stats.mos.ru/counter.js', array(), NULL, true );

    // если это страница филиала - подгрузить yandex maps api
    if (is_singular('mm_branch')) {
      wp_enqueue_script( 'yandex-maps-api', 'https://api-maps.yandex.ru/2.1/?lang=ru_RU', array(), '2.1', false );
    }

    // если это страница филиала - подгрузить yandex maps api
    if (is_singular('mm_service') or is_singular('mm_department')) {

      // решил в кучку собрать и минифицировать, пока только js

      // mm_enqueue_local_script('magnific-popup-js', '/js/jquery.magnific-popup.min.js', array('jquery'), true);
      // mm_enqueue_local_script('init-magnific-popup-js', '/js/init.magnific-popup.js', array('magnific-popup-js'), true);

      mm_enqueue_local_style('magnific-popup', '/stylesheets/css/magnific-popup.css');
    }

    if( !is_admin()){
      wp_deregister_script('jquery');
      wp_register_script('jquery', ("http://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"), true, '3.1.0');
      wp_enqueue_script('jquery');
    }

  }

  add_filter( 'script_loader_tag', function ( $tag, $handle ) {

    if ( 'dzm-counter' !== $handle )
    return $tag;
    return str_replace( ' src', ' defer="defer" async="true" src', $tag );
  }, 10, 2 );

  add_action( 'wp_enqueue_scripts', 'mm_scripts_and_styles' );
