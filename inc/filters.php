<?php

function mv_target_blank_for_link_post_formats( $atts, $item, $args ) {

  if ( has_post_format( 'link', $item->object_id )):
    $atts['target'] = '_blank';

    $page_text = get_page($item->object_id)->post_content;

    // https://css-tricks.com/snippets/php/find-urls-in-text-make-links/

    $reg_exUrl = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";
    if(preg_match($reg_exUrl, $page_text, $url)):
      $filtered_url = $url[0];
    else:
      $filtered_url = '';
    endif;

    $atts['href'] = $filtered_url;
  endif;
  return $atts;
}

add_filter( 'nav_menu_link_attributes', 'mv_target_blank_for_link_post_formats', 10, 3 );

function mv_add_ext_icon_for_link_post_formats($item_output, $item, $depth, $args) {

  if ( has_post_format( 'link', $item->object_id )):
    // return preg_replace('/<a>(.*)<\/a>/', '<a>$1++</a>', $item_output);
    $ext_symbol =  '<i class="fa fa-external-link" aria-hidden="true"></i>';
    return preg_replace('/>(.*)<\/a>/', '>$1 ' . $ext_symbol .'</a>', $item_output);
  endif;

  return $item_output;
}
add_filter('walker_nav_menu_start_el', 'mv_add_ext_icon_for_link_post_formats', 10, 4);

/**
* Добавление логотипа в меню
**/

// add_filter( 'wp_nav_menu_items', 'mm_add_logo_to_menu', 10, 2 );
// function mm_add_logo_to_menu ( $items, $args ) {
//
//   $logo_id = get_theme_mod('logo_dark_on_light');
//   $logo_img_obj = new Mm_attachment($logo_id);
//
//   if ($args->theme_location == 'header-menu') {
//
//     $site_desc = get_bloginfo('url');
//       $logo = '<li class="logo--top show-for-large"><a href="' . get_bloginfo( 'url' ) .'">' . $logo_img_obj->show() .'</a></li>';
//       $items = $logo . $items;
//   }
//     return $items;
// }

/**
* Форма комментариев
**/

function mm_url2phone($str) {
  $str = str_replace('Сайт', 'Телефон', $str);
  $str = str_replace('Website', 'Phone', $str);
  return $str;

}
add_filter('comment_form_field_url', 'mm_url2phone');

/**
* Название в поле title и на баннере
**/

// для title в head страницы
function mm_wp_title_filter() {

  if (is_404()):
    $title_str = "Страница не найдена";
  elseif (is_archive()):
    $title_str = post_type_archive_title( "", false );
  elseif (is_front_page() or is_home()):
    return get_bloginfo('name');
  else:
    $title_str = get_the_title();
  endif;

  return $title_str . ' &raquo; ' . get_bloginfo();
}

add_filter('wp_title', 'mm_wp_title_filter');

// на баннере
function mm_title_on_hero_fn($title) {

  if (is_404()):
    $title_str = "Страница не найдена";
  elseif (is_archive()):
    $title_str = post_type_archive_title( "", false );
  elseif (is_front_page() or is_home()):
    $title_str = get_bloginfo('name');
  else:
    $title_str = $title;
  endif;

  return $title_str;
}

add_filter('mm_title_on_hero', 'mm_title_on_hero_fn');
