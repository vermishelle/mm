<?php
// https://gist.github.com/morganestes/5486338

// работает как в Loop'е, так и без

function mm_get_the_ID() {
  if ( in_the_loop() ) {
    $post_id = get_the_ID();
  } else {
    /** @var $wp_query wp_query */
    global $wp_query;
    $post_id = $wp_query->get_queried_object_id();
    }
  return $post_id;
}
