<?php

//[addresses_covered]
function addresses_covered_function(){


	$args = array(
	'posts_per_page'   => -1,
	'offset'           => 0,
	'category'         => '',
	'category_name'    => '',
	'orderby'          => 'menu_order',
	// 'orderby'          => 'title',
	'order'            => 'DESC',
	'include'          => '',
	'exclude'          => '',
	'meta_key'         => '',
	'meta_value'       => '',
	'post_type'        => 'mm_branch',
	'post_mime_type'   => '',
	'post_parent'      => 0,
	'author'	   => '',
	'post_status'      => 'publish',
	'suppress_filters' => true
	);

	$branches_array = get_posts( $args ); ?>

	<?php foreach ($branches_array as $branch) { ?>
		<h3>Адреса, обслуживаемые <?php echo $branch->post_title ?></h3>
		<?php // var_dump( $branch->ID); ?>
		<?php echo get_post_meta($branch->ID, 'adresses_covered')[0]; ?>


	<?php }
}

add_shortcode( 'addresses_covered', 'addresses_covered_function' );

// [lorem]

function lorem_shortcode_function() {

	return "<p>Данный текст является примером и присутствует здесь только потому, что необходимый контент не готов, а некоторое количество текста все равно необходимо для правильного восприятия дизайна. Этот текст скоро будет удален, как только будет предоставлен необходимый контент.<br>Если вы видите этот текст на работающем сайте, пожалуйста, напишите администратору сайта, поскольку такого быть не должно.</p>";
}

add_shortcode( 'lorem', 'lorem_shortcode_function' );

// [mm_administration]
function mm_administration() {
  $mm_org = new Mm_org();
  foreach ($mm_org->branches() as $branch) {
    mm_show_branch_admins_schedule($branch);
  };
}

add_shortcode('mm_administration', 'mm_administration');

//[mm_addresses_and_phones]
function mm_addresses_and_phones_function(){

	$org = new Mm_org();
	$branches = $org->branches();

	foreach ($branches as $branch) { ?>
		<h3><?php echo $branch->title() ?></h3>
		<h4>Адрес</h4>
		<?php echo $branch->adress() ?>

		<?php if ( !empty($branch->phones()) or sizeof($branches) == 1 && !empty($org->phones()) ) { ?>
			<h4>Контактная информация</h4>
			<?php echo $branch->show_contacts_table();
		}


	} // конец перебора филиалов
} // конец функции

add_shortcode( 'mm_addresses_and_phones', 'mm_addresses_and_phones_function' );

//[mm_search]
function mm_search_function(){
	get_search_form();
} // конец функции

add_shortcode( 'mm_search', 'mm_search_function' );
