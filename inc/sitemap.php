<?php function mm_show_posttype_for_sitemap($mm_post_type) {

	$mm_post_type_object = get_post_type_object($mm_post_type); ?>

	<h2><?php echo $mm_post_type_object->labels->name; ?></h2>

	<ul>

		<?php $args = array(
		'posts_per_page'   => -1,
		'offset'           => 0,
		'category'         => '',
		'category_name'    => '',
		// 'orderby'          => 'menu_order',
		'orderby'          => 'title',
		'order'            => 'DESC',
		'include'          => '',
		'exclude'          => '',
		'meta_key'         => '',
		'meta_value'       => '',
		'post_type'        => $mm_post_type,
		'post_mime_type'   => '',
		'post_parent'      => 0,
		'author'	   => '',
		'post_status'      => 'publish',
		'suppress_filters' => true
		);

		$posts_array = get_posts( $args );

		foreach ($posts_array as $mm_post) { ?>

		<?php // var_dump($mm_post); ?>

		<li><a href="<?php echo get_permalink($mm_post->ID) ?>"><?php echo $mm_post->post_title ?></a></li>

		<?php // здесь еще про детишек

$args = array(
	'post_type' => $mm_post_type,
	'child_of' => $mm_post->ID,
	'title_li' => '',
	'echo' => false,
);

if ( wp_list_pages( $args ) ) { ?>
	<ul>
	<?php echo wp_list_pages( $args ); ?>
	</ul>
<?php }



		 } // конец перебора постов вехнего уровня ?>

	</ul>

<?php }

function mm_show_posttype_for_sitemap_with_ids($mm_post_type) {

	$mm_post_type_object = get_post_type_object($mm_post_type); ?>

	<h2><?php echo $mm_post_type_object->labels->name; ?></h2>

	<ul>

		<?php $args = array(
		'posts_per_page'   => -1,
		'offset'           => 0,
		'category'         => '',
		'category_name'    => '',
		// 'orderby'          => 'menu_order',
		'orderby'          => 'title',
		'order'            => 'DESC',
		'include'          => '',
		'exclude'          => '',
		'meta_key'         => '',
		'meta_value'       => '',
		'post_type'        => $mm_post_type,
		'post_mime_type'   => '',
		'post_parent'      => 0,
		'author'	   => '',
		'post_status'      => 'publish',
		'suppress_filters' => true
		);

		$posts_array = get_posts( $args );

		foreach ($posts_array as $mm_post) { ?>

		<?php // var_dump($mm_post); ?>

		<li><a href="<?php echo get_permalink($mm_post->ID) ?>"><?php echo $mm_post->post_title ?></a> (код <?php echo $mm_post->ID ?>)</li>

		<?php // здесь еще про детишек

$args = array(
	'post_type' => $mm_post_type,
	'child_of' => $mm_post->ID,
	'title_li' => '',
	'echo' => false,
);

if ( wp_list_pages( $args ) ) { ?>
	<ul>
	<?php echo wp_list_pages( $args ); ?>
	</ul>
<?php }



		 } // конец перебора постов вехнего уровня ?>

	</ul>

<?php }
