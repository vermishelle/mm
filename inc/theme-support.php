<?php

/**
 * Enables the Excerpt meta box in Page edit screen.
 */
function musweb_add_excerpt_support_for_pages() {
	add_post_type_support( 'page', 'excerpt' );
}
add_action( 'init', 'musweb_add_excerpt_support_for_pages' );

function musweb_add_theme_support() {
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'title-tag' );
	add_theme_support( 'html5');
	add_theme_support('automatic-feed-links');

	add_theme_support( 'post-formats', array( 'link' ));
	add_post_type_support( 'page', 'post-formats' );
}
add_action( 'after_setup_theme', 'musweb_add_theme_support' );


$defaults = array(
	'default-image'          => '',
	'random-default'         => false,
	'width'                  => 0,
	'height'                 => 0,
	'flex-height'            => false,
	'flex-width'             => false,
	'default-text-color'     => '',
	'header-text'            => true,
	'uploads'                => true,
	'wp-head-callback'       => '',
	'admin-head-callback'    => '',
	'admin-preview-callback' => '',
);
add_theme_support( 'custom-header', $defaults );
