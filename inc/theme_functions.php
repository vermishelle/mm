<?php

function mm_enqueue_local_script($handle, $path, $deps = array(), $in_footer=false) {
  wp_enqueue_script( $handle, get_template_directory_uri() . $path, $deps, filemtime(get_template_directory() . $path), $in_footer );

}

function mm_enqueue_local_style($handle, $path, $deps = array()) {
  wp_enqueue_style( $handle, get_template_directory_uri() . $path, $deps, filemtime(get_template_directory() . $path));
}
