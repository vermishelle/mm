<?php

// start решение проблемы, когда картинки меньшего чем thumbnail размера не кропятся,
// и соответственно пропорции не те
// отсюда http://alxmedia.se/code/2013/10/thumbnail-upscale-correct-crop-in-wordpress/

/*  Thumbnail upscale
/* ------------------------------------ */
// function alx_thumbnail_upscale( $default, $orig_w, $orig_h, $new_w, $new_h, $crop ){
//     if ( !$crop ) return null; // let the wordpress default function handle this

//     $aspect_ratio = $orig_w / $orig_h;
//     $size_ratio = max($new_w / $orig_w, $new_h / $orig_h);

//     $crop_w = round($new_w / $size_ratio);
//     $crop_h = round($new_h / $size_ratio);

//     $s_x = floor( ($orig_w - $crop_w) / 2 );
//     $s_y = floor( ($orig_h - $crop_h) / 2 );

//     return array( 0, 0, (int) $s_x, (int) $s_y, (int) $new_w, (int) $new_h, (int) $crop_w, (int) $crop_h );
// }
// add_filter( 'image_resize_dimensions', 'alx_thumbnail_upscale', 10, 6 );
// end crop solution

// поскольку это решение конфликтует с ручной обрезкой (она сбивается), пока закоментировал и убрал загрузку из functions.php