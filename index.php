<!-- this is index.php -->
<?php
get_header();
$obj = new Mm_post(get_the_ID());
get_template_part( 'template-parts/all', 'hero_banner' ); ?>

<div class="row padding-b-l">
  <div class="column small-12">
    <?php custom_breadcrumbs(); ?>
  </div>

  <?php if ($obj->has_thumbnail() && !$obj->hide_thumbnail()): ?>
    <div class="column small-12 medium-5 large-4">
      <?php echo $obj->thumbnail()->show('padding-b-s full-width', '1x1-s'); ?>
    </div>
    <div class="column small-12 medium-7 large-8"><?php echo $obj->content(); ?></div>
  <?php else: ?>
    <div class="column small-12"><?php echo $obj->content(); ?></div>
  <?php endif; ?>
  <div class='column small-12'>
    <?php comments_template(); ?>
  </div>
</div>

<?php

get_sidebar();
get_footer();
