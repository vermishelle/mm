<!-- search.php -->
<?php

$search_query = get_search_query();

get_header(); ?>

<!-- выводим тело страницы -->

<div class="row column">
  <?php custom_breadcrumbs(); ?>

  <?php if (have_posts()) :
    echo "<p>Результат поиска по запросу \"<strong>$search_query</strong>\":";
     while (have_posts()) :
        the_post();
          ?><h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2><?php
          the_excerpt();
     endwhile;
   else:
     echo "<p>По вашему запросу \"<strong>$search_query</strong>\" результатов не найдено. Попробуйте повторить поиск:</p>";
     get_search_form();
  endif; ?>
</div>



<?php
get_sidebar();
get_footer();
