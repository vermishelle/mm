<?php

$search_query = get_search_query();
$home_url = home_url();

$form = <<<HEREDOC
<form role="search"
      method="get"
      class="search-form"
      action="$home_url">
  <div class="input-group">
    <input type="search"
           class="input-group-field"
           placeholder="Поиск по сайту"
           value="$search_query"
           name="s">
    <div class="input-group-button">
      <input type="submit" class="button" value="Поиск">
    </div>
  </div>
</form>
HEREDOC;

echo $form;
