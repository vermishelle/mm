<!-- this is single-mm_branch.php -->

<?php $mm_branch = new Mm_branch(get_the_ID());
$org = new Mm_org(); ?>

<?php get_header(); ?>

<?php get_template_part( 'template-parts/all', 'hero_banner' ); ?>

<div class="margin-b-l">

	<section class="row column">
	  <?php custom_breadcrumbs(); ?>
	</section>

	<?php if (!empty($mm_branch->content(false))): ?>
		<section class="row column">
			<h2>О <?php echo $mm_branch->title()?></h2>
			<?php echo $mm_branch->content(); ?>
		</section>
	<?php endif; ?>

	<?php if ($mm_branch->adress()): ?>
		<section class="row column" id='adress'>
	  	<h2>Адрес</h2>
	  	<p><?php echo $mm_branch->adress();?></p>
	  	<h3>Карта</h3>
			<?php mm_show_ya_map($mm_branch->adress(), $mm_branch->title()); ?>

			<?php if (!empty($mm_branch->shema_proezda())): ?>
				<h3>Схема проезда</h3>
				<p>
					<?php echo $mm_branch->shema_proezda(); ?>
				</p>
			<?php endif; ?>
		</section>
		<?php endif;

		    if  ( $mm_branch->head() ) { ?>
					<section class="row column">
		      	<h2>Руководитель</h2>
		      	<p><?php echo $mm_branch->head()->position() ?> <a href="<?php echo $mm_branch->head()->url() ?>"><?php echo $mm_branch->head()->title() ?></a></p>
					</section>
		    <?php } // конец проверки на наличие главы филиала


		      if  ( $mm_branch->working_time() ) { ?>
						<section class="row column">
		        	<h2>Время работы</h2>
		        	<p><?php echo $mm_branch->working_time() ?></p>
						</section>
		      <?php } ?>

					<section class="row column">
					<?php mm_print('Контакты', $mm_branch->show_contacts_table(), 'h2', '', $mm_branch->phones()); ?>
				</section>

				<?php if ($mm_branch->administration()): ?>
					<section class="row column">
					<h2>Администрация</h2>
					<?php // var_dump($phones_array);
					foreach ($mm_branch->administration() as $employee) { ?>

						<p class="margin-b-0"><strong><?php echo ($employee->position() . ": ") ?></strong><a href="<?php echo $employee->url(); ?>">
						<?php echo $employee->title(); ?></a></p>

					<?php } ?>
					</section>
				<?php endif; ?>

		      <?php if  ( $mm_branch->adresses_covered() ) { ?>
						<section class="row column" id='adresses_covered'>
		        	<h2>Адреса, обслуживаемые по ОМС</h2>
		        	<?php echo $mm_branch->adresses_covered() ?>
						</section>
		      <?php }

$number_of_departments_of_the_branch = sizeof($mm_branch->departments());

	if ($number_of_departments_of_the_branch > 0 and sizeof($org->branches()) > 1) : ?>

<section class="row">
	<div class="column small-12">
	<h2>Отделения, работающие в <?php echo $mm_branch->title(); ?></h2>
</div>
	    <?php foreach ($mm_branch->departments() as $department) {

	      // если отделение корневое или не корневое если корневое НЕ чекнуто
	      if (!$department->hide_from_lists() and $department->is_a_root() || !$department->root()->belongs_to_branch($mm_branch)  ) {   ?>

	        <div class="column small-12 medium-4 large-3 padding-on-small padding-bottom">
	          <a href="<?php echo $department->url(); ?>">
	            <?php echo mm_show_thumbnail_with_placeholder_if_empty($department->id, "4x3", "full-width"); ?>
	            <div class="bg-accent padding text-center">
	              <strong class="uppercase white-text"><?php echo $department->title(); ?></strong>
	            </div>
	          </a>
					</div>
	      <?php }
	    } ?>

	</section>

		<?php // кнопку-переход к общему списку показываем только если те отделения что показаны в филиале не есть все отделения что есть (то есть прячем кнопку если показанный список отделений и так общий)

		if ($number_of_departments_of_the_branch < sizeof($org->departments())): ?>

		  <div class="row column">
		        <p><a class="button button-with-icon--left margin-top-l" href="<?php echo (get_post_type_archive_link('mm_department')) ?>"><i class="fa fa-search fa-fw"></i>Общий список отделений поликлиники</a></p>
		  </div>
		<?php endif; // условие показа кнопки ?>

<?php endif; // показ блока отделений только если они есть

get_sidebar();
get_footer();
