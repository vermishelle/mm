<!-- this is single-mm_department.php -->
<?php get_header(); ?>

<?php $org = new Mm_org(); ?>
<?php $dep = new Mm_department(get_the_ID()); ?>

<?php get_template_part( 'template-parts/all', 'hero_banner' ); ?>

<!-- выводим тело страницы -->
<div class="row column">
  <?php custom_breadcrumbs(); ?>
</div>


<?php if (!empty($dep->gallery())): ?>
  <section class="row column">
    <h2>Об отделении</h2>
    <div class="float-right padding-left">
      <?php $dep->show_gallery(); ?>
    </div>
    <div class="clearfix">
      <?php echo $dep->content(); ?>
    </div>
  </section>
<?php else: ?>
  <section class="row column">
    <h2>Об отделении</h2>
    <?php echo $dep->content(); ?>
  </section>
<?php endif; // конец if empty gallery ?>

<div class="row column">
  <?php if ($dep->head()) mm_print(
    'Заведующий отделением',
    sprintf('<a href="%2$s">%1$s</a>', $dep->head()->title(), $dep->head()->url()),
    'h2', 'p');

  mm_print('Телефоны', show_contacts_table($dep->phones()), 'h2', 'p', $dep->phones()); ?>

</div>

<?php if (!empty($dep->children())): ?>
  <section class="row column">
    <h2>Отделения данного типа</h2>
    <?php if (sizeof($org->branches()) > 1): ?>
      <?php foreach ($dep->children() as $child) { ?>
        <p><strong><a href="<?php echo $child->url() ?>"><?php echo $child->title() ?></a></strong> (работает в <a href="<?php echo $child->branches()[0]->url() ?>"><?php echo $child->branches()[0]->title() ?>)</a></p>
      <?php }
    else:
      foreach ($dep->children() as $child): ?>
        <p><strong><a href="<?php echo $child->url() ?>"><?php echo $child->title() ?></a></strong></p>
      <?php endforeach;
    endif; ?>
  </section>
<?php endif;

$dep_services = $dep->services();
if (!empty($dep_services)) : // если есть услуги ?>
  <section class="bg-grey">
    <div class="row column">
      <?php if( !empty($dep->children()) ): ?>
        <h2>Услуги, предоставляемые в отделениях</h2>
      <?php else:
        ?><h2>Услуги, предоставляемые в отделении</h2>
      <?php endif; ?>
      <?php mm_show_services_in_circles($dep_services); ?>
    </div>
  </section> <!-- колонка -->
<?php endif; ?>


<?php $dep_employees = $dep->employees();
// если это родительский департамент - суммируем сотрудников этого отделения
// с сотрудниками дочерних отделений, затем оставляем только уникальных.
// ведь еся Иванов работает в 1-м терапевтическом отделении, то в "терапевтических
// отделениях" он же тоже работает

if ($dep->is_a_root() and !empty($dep->children())):
  foreach ($dep->children() as $child_dep):
    $dep_employees = array_merge ($dep_employees, $child_dep->employees());
  endforeach;

  $dep_employees = array_unique($dep_employees, SORT_REGULAR);
endif;

if (!empty($dep_employees)): ?>

  <section class="row">
    <div class="column small-12">
      <h2>Сотрудники, работающие в отделении</h2>
    </div>

    <?php foreach ($dep_employees as $dep_employee): ?>
      <div class="column text-center small-8 medium-4 large-3 medium-offset-0 small-offset-2 end padding-bottom">
        <a href="<?php echo $dep_employee->url(); ?>">
          <?php echo $dep_employee->thumbnail()->show('full-width', '3x4-s'); ?>
          <div class="bg-accent padding">
            <strong class="uppercase white-text"><?php echo $dep_employee->title(); ?></strong>
          </div>
        </a>
      </div>
    <?php endforeach; ?>
  </section>
<?php endif;

comments_template();
get_sidebar();
get_footer();
