<!-- this is single-mm_employee.php -->
<?php get_header();

$empl = new Mm_employee(get_the_ID()); ?>

<section class="bg-grey padding-bottom">

  <div class="row column padding-tb-s">
    <?php custom_breadcrumbs(); ?>
  </div>

  <div class="row padding-tb-xl">
    <div class="padding-on-small column large-4 medium-5 small-12">
      <div class="padding-s bg-white margin text-center">
        <?php echo $empl->thumbnail()->show('full-width padding-bottom', '3x4-m'); ?>
        <p>
          <strong><?php echo $empl->title() ?></strong>
          <br>
          <?php echo $empl->position() ?>
        </p>
      </div>
    </div>
    <div class="column large-8 medium-7 small-12">
      <?php if (!empty($empl->schedule())): ?>
        <h2>Расписание приема</h2>
        <p><?php // чтоб переносы строк не терялись - обернуто в wpautop
        echo wpautop($empl->schedule()); ?></p>
      <?php endif; ?>

      <?php $phones = $empl->phones();

      if(!empty($phones)): ?>
        <h2>Контактная информация</h2>
        <?php echo show_contacts_table($phones); ?>
      <?php endif; ?>

      <?php $sertificate_data_filled = !empty($empl->spec_sertificate_educational_institution()) or !empty($empl->spec_sertificate_date_received()) or !empty($empl->spec_sertificate_expiration()) or !empty($empl->spec_sertificate_speciality());

      if($sertificate_data_filled): ?>

        <h2>Сведения из сертификата специалиста</h2>
        <?php if (!empty($empl->sertificate_number())): ?>
          <p>Номер сертификата: <?php echo $empl->sertificate_number(); ?></p>
        <?php endif; ?>

        <?php if (!empty($empl->spec_sertificate_educational_institution())): ?>
          <p>Учебное заведение: <?php echo $empl->spec_sertificate_educational_institution(); ?></p>
        <?php endif; ?>

        <?php if (!empty($empl->spec_sertificate_date_received())): ?>
          <p>Дата получения сертификата: <?php echo $empl->spec_sertificate_date_received(); ?></p>
        <?php endif; ?>

        <?php if (!empty($empl->spec_sertificate_expiration())): ?>
          <p>Сертификат действителен до: <?php echo $empl->spec_sertificate_expiration(); ?></p>
        <?php endif; ?>

        <?php if (!empty($empl->spec_sertificate_speciality())): ?>
          <p>Специальность по сертификату: <?php echo $empl->spec_sertificate_speciality(); ?></p>
        <?php endif; ?>

      <?php endif; ?>

      <?php $category_data = !empty($empl->category()) or !empty($empl->category_specialty()) or !empty($empl->category_assignment_date());

      if($category_data): ?>

        <h2>Квалификационная категория</h2>

        <?php if (!empty($empl->category())): ?>
          <p>Категория: <?php echo $empl->category(); ?></p>
        <?php endif; ?>

        <?php if (!empty($empl->category_specialty())): ?>
          <p>Специальность: <?php echo $empl->category_specialty(); ?></p>
        <?php endif; ?>

        <?php if (!empty($empl->category_assignment_date())): ?>
          <p>Дата присвоения: <?php echo $empl->category_assignment_date(); ?></p>
        <?php endif; ?>

      <?php endif; ?>

      <?php if(!empty($empl->resume())):
          echo apply_filters( 'the_content', $empl->resume() );
      endif; ?>

      <?php if (empty($empl->schedule()) and empty($phones) and !$sertificate_data_filled and !$diploma_data and !$category_data): ?>
        <p>Карточка сотрудника в процессе заполнения.</p>
      <?php endif; ?>

    </div>
  </div>
</section>

<?php comments_template();
get_sidebar();
get_footer();
