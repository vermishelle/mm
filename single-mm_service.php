<!-- this is single-mm_service.php -->
<?php get_header(); ?>

<?php get_template_part( 'template-parts/all', 'hero_banner' );

$srv_obj = new Mm_service(get_the_ID());

$mm_service_pod = pods( 'mm_service', get_the_id() );
$pricelist_positions_array = $mm_service_pod->field( 'prices' );?>

<nav class="row column">
  <?php custom_breadcrumbs(); ?>
</nav>


<section class="row column">
  <h2>Подробнее</h2>

  <?php if (!empty($srv_obj->gallery())): ?>
    <div class="row column">
      <div class="float-right padding-left">
        <?php $srv_obj->show_gallery(); ?>
      </div>
      <?php echo $srv_obj->content(); ?>
    </div>

    <?php else: ?>
      <div class="row column">
        <?php echo $srv_obj->content(); ?>
      </div>
  <?php endif; // конец if empty gallery ?>
</section>

<?php //показываем секцию pricelist если в кастомайзере задана страница с прайслистом или
//  заполнены CPT pricelist, относящиеся к данной услуге

$show_pricelist_section = !$srv_obj->free_service() && (!empty($srv_obj->pricelist()) || !empty(get_theme_mod('pricelist_page')));


if (false): // $show_pricelist_section - temp disable ?> 
  <section class="row column">
    <h2>Прейскурант</h2>
    <?php if (!empty($srv_obj->pricelist())):
      $srv_obj->show_pricelist_table();
    else: ?>
      <p>С ценами на услуги вы можете ознакомиться в <a target="_blank" href="<?php echo get_theme_mod('pricelist_page') ?>">общем прейскуранте</a> поликлиники.</p>
    <?php endif; ?>
  </section> <!-- конец small-12 колонки с прайсом -->
<?php endif; // end if $pricelist_positions_array


if ($srv_obj->has_phones()): ?>
  <section class="row column">
    <h2>Контактная информация</h2>
    <?php echo $srv_obj->show_contacts_table(); ?>
  </section>
<?php endif; ?>

<div class="row column"> <!-- текст под иконками услуг -->
  <a class="button button-with-icon--left margin-top-l" href="<?php echo (get_post_type_archive_link('mm_service')) ?>"><i class="fa fa-search fa-fw"></i> Посмотреть другие услуги</a>
</div>

<?php

comments_template();
get_sidebar();
get_footer();
