<!-- single-vacancies.php -->
<?php get_header();

get_template_part( 'template-parts/all', 'hero_banner' ); ?>

<!-- выводим тело страницы -->

<div class="row column padding-b-l">
  <?php custom_breadcrumbs(); ?>

  <?php if (have_posts()) :
    while (have_posts()) :
        the_post(); ?>
            <p>В <?php echo get_theme_mod(short_name) ?> открыта вакансия "<?php echo the_title(); ?>".</p>
    		<p>Количество открытых вакансий данного типа: <?php echo pods('vacancies', get_the_ID())->field('number'); ?>.</p>

    		  <?php if (get_the_content()) { ?>
    		  	<h2>О вакансии</h2>
			    <?php the_content();
			  }; ?>
        <a href="<?php echo get_post_type_archive_link('vacancies') ?>"><< Вернуться к общему списку вакансий</a>
    <?php endwhile;
  endif; ?>
</div>



<?php
get_sidebar();
get_footer();
