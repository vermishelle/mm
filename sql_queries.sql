-- чтоб группировалось без проблем
-- http://stackoverflow.com/questions/34115174/i-am-getting-an-error-in-mysql-related-to-only-full-group-by-when-executing-a-qu
set global sql_mode='STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
set session sql_mode='STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';

-- прайслист по конкретной услуге (щдесь - ID = 176)

SET @service_id = 176;

SELECT prices.menu_order AS 'menu_order', service.post_title AS 'service_title', prices.post_title AS 'pricelist_position_title', artice.meta_value AS 'article', subsection.meta_value AS 'subsection', oms_lkg.meta_value AS 'oms_lkg', department.meta_value AS 'department', price.meta_value AS 'price'
  FROM wp_podsrel AS rel
  JOIN wp_posts AS service
    ON rel.item_id = service.ID
    AND service.ID = @service_id
  JOIN wp_posts AS prices
    ON rel.related_item_id = prices.ID
  JOIN wp_postmeta AS artice
    ON prices.ID = artice.post_id and artice.meta_key = 'article'
  JOIN wp_postmeta AS subsection
    ON prices.ID = subsection.post_id and subsection.meta_key = 'subsection'
  JOIN wp_postmeta AS oms_lkg
    ON prices.ID = oms_lkg.post_id and oms_lkg.meta_key = 'oms_lkg'
  JOIN wp_postmeta AS department
    ON prices.ID = department.post_id and department.meta_key = 'department'
  JOIN wp_postmeta AS price
    ON prices.ID = price.post_id and price.meta_key = 'mm_price';

  -- общий прайслист, сгруппировать как-то пока не получилось

  SELECT prices.menu_order AS 'menu_order', service.post_title AS 'service_title', prices.post_title AS 'pricelist_position_title', artice.meta_value AS 'article', subsection.meta_value AS 'subsection', oms_lkg.meta_value AS 'oms_lkg', department.meta_value AS 'department', price.meta_value AS 'price'
    FROM wp_podsrel AS rel
    JOIN wp_posts AS service
      ON rel.item_id = service.ID
    JOIN wp_posts AS prices
      ON rel.related_item_id = prices.ID
    JOIN wp_postmeta AS artice
      ON prices.ID = artice.post_id and artice.meta_key = 'article'
    JOIN wp_postmeta AS subsection
      ON prices.ID = subsection.post_id and subsection.meta_key = 'subsection'
    JOIN wp_postmeta AS oms_lkg
      ON prices.ID = oms_lkg.post_id and oms_lkg.meta_key = 'oms_lkg'
    JOIN wp_postmeta AS department
      ON prices.ID = department.post_id and department.meta_key = 'department'
    JOIN wp_postmeta AS price
      ON prices.ID = price.post_id and price.meta_key = 'mm_price';

-- pricelist has ОМС/ЛКГ
-- услуга (пост) -> JOIN -> строка прайслиста (пост, связь через podsrel) -> JOIN wp_postmeta (meta_key = 'oms_lkg)

-- конвертировать в префикс и убедиться что работает!

"SELECT service.ID FROM wp_posts AS service WHERE service.ID = $this->id
  JOIN wp_podsrel AS rel ON rel.item_id = service.ID
  JOIN wp_posts AS pricelist_positions ON pricelist_positions.ID = rel.related_item_id
  JOIN wp_postmeta AS oms_lkg ON pricelist_positions.ID = oms_lkg.post_id
    WHERE oms_lkg.meta_key = 'oms_lkg' AND oms_lkg.meta_value IS NOT NULL AND oms_lkg.meta_value != ''"


    SELECT service.post_title, pricelist_positions.post_title FROM wp_posts AS service -- WHERE service.ID = 172
      JOIN wp_podsrel AS rel ON rel.item_id = service.ID
      JOIN wp_posts AS pricelist_positions ON pricelist_positions.ID = rel.related_item_id
      JOIN wp_postmeta AS oms_lkg ON pricelist_positions.ID = oms_lkg.post_id
        -- WHERE oms_lkg.meta_key = 'oms_lkg' AND oms_lkg.meta_value IS NOT NULL AND oms_lkg.meta_value != '';
