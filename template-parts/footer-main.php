<?php

$head = new Mm_employee(get_theme_mod('head'));
$head_url = $head->url();
$head_position = $head->position();
$head_title = $head->title();

// $socials =

$menu_args1 = array(
  'theme_location'  => 'footer1',
  'menu'            => '',
  'container'       => '',
  'container_class' => '',
  'container_id'    => '',
  'menu_class'      => 'no-bullet',
  'menu_id'         => '',
  'echo'            => true,
  'fallback_cb'     => 'wp_page_menu',
  'before'          => '',
  'after'           => '',
  'link_before'     => '',
  'link_after'      => '',
  'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
  'depth'           => 1,
  'walker'          => ''
);

$menu_args2 = array(
  'theme_location'  => 'footer2',
  'menu'            => '',
  'container'       => '',
  'container_class' => '',
  'container_id'    => '',
  'menu_class'      => 'no-bullet',
  'menu_id'         => '',
  'echo'            => true,
  'fallback_cb'     => 'wp_page_menu',
  'before'          => '',
  'after'           => '',
  'link_before'     => '',
  'link_after'      => '',
  'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
  'depth'           => 1,
  'walker'          => ''
);

$menu_args3 = array(
  'theme_location'  => 'footer3',
  'menu'            => '',
  'container'       => '',
  'container_class' => '',
  'container_id'    => '',
  'menu_class'      => 'no-bullet',
  'menu_id'         => '',
  'echo'            => true,
  'fallback_cb'     => 'wp_page_menu',
  'before'          => '',
  'after'           => '',
  'link_before'     => '',
  'link_after'      => '',
  'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
  'depth'           => 1,
  'walker'          => ''
);

$menu_args4 = array(
  'theme_location'  => 'footer4',
  'menu'            => '',
  'container'       => '',
  'container_class' => '',
  'container_id'    => '',
  'menu_class'      => 'no-bullet',
  'menu_id'         => '',
  'echo'            => true,
  'fallback_cb'     => 'wp_page_menu',
  'before'          => '',
  'after'           => '',
  'link_before'     => '',
  'link_after'      => '',
  'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
  'depth'           => 1,
  'walker'          => ''
);

 ?>

<footer class="bg-complimentary-dark">

  <?php $mm_all_user_created_menus = wp_get_nav_menus();
  $mm_all_menu_locations_in_theme = get_nav_menu_locations();

  // считаем сколько в футере работающих меню
  static $counter = 0;
  foreach ($mm_all_menu_locations_in_theme as $menu_place_name => $menu_id):
    if ($menu_place_name !='header-menu' &&
        $menu_place_name !='footer-left' && // wierd bug on 66 workaround
        $menu_place_name !='footer-center' && // wierd bug on 66 workaround
        $menu_id != 0):
      $counter++;
    endif;
  endforeach;

  switch ($counter):
    case 0:
      $cl = 'row';
      break;
    case 1:
      $cl = 'row';
      break;
    case 2:
      $cl = 'row small-up-1 medium-up-2';
      break;
    case 3:
      $cl = 'row small-up-1 medium-up-3';
      break;
    default:
      $cl = 'row small-up-1 medium-up-2 large-up-4';
      break;
  endswitch; ?>

  <div class="<?php echo $cl ?> padding-bottom-l">

  <?php if (array_key_exists ("footer1", $mm_all_menu_locations_in_theme) and $mm_all_menu_locations_in_theme['footer1'] != 0): ?>


    <div class="column">
      <!-- первая колонка-->
      <h4><?php
      $mm_assigned_menu = $mm_all_menu_locations_in_theme["footer1"];
      $mm_assigned_menu_name = (wp_get_nav_menu_object($mm_assigned_menu)->{'name'});
      echo $mm_assigned_menu_name ?></h4>
      <?php wp_nav_menu( $menu_args1 ); ?>
    </div>

  <?php endif; ?>

  <?php if (array_key_exists ("footer2", $mm_all_menu_locations_in_theme) and $mm_all_menu_locations_in_theme['footer2'] != 0) { ?>

    <div class="column">
      <!-- Вторая колонка-->
      <h4><?php
      $mm_assigned_menu = $mm_all_menu_locations_in_theme["footer2"];
      $mm_assignerd_menu_name = (wp_get_nav_menu_object($mm_assigned_menu)->{'name'});
      echo $mm_assignerd_menu_name ?></h4>
      <?php wp_nav_menu( $menu_args2 ); ?>
    </div>

  <?php } ?>

  <?php if (array_key_exists ("footer3", $mm_all_menu_locations_in_theme) and $mm_all_menu_locations_in_theme['footer3'] != 0) { ?>

    <div class="column">
      <!-- третья колонка-->
      <h4><?php
      $mm_assigned_menu = $mm_all_menu_locations_in_theme["footer3"];
      $mm_assignerd_menu_name = (wp_get_nav_menu_object($mm_assigned_menu)->{'name'});
      echo $mm_assignerd_menu_name ?></h4>
      <?php wp_nav_menu( $menu_args3 ); ?>
    </div>

  <?php } ?>

  <?php if (array_key_exists ("footer4", $mm_all_menu_locations_in_theme) and $mm_all_menu_locations_in_theme['footer4'] != 0) { ?>

    <div class="column">
      <!-- третья колонка-->
      <h4><?php
      $mm_assigned_menu = $mm_all_menu_locations_in_theme["footer4"];
      $mm_assignerd_menu_name = (wp_get_nav_menu_object($mm_assigned_menu)->{'name'});
      echo $mm_assignerd_menu_name ?></h4>
      <?php wp_nav_menu( $menu_args4 ); ?>
    </div>

  <?php } ?>

  </div>
  <div class="bottom-footer bg-complimentary-darkest">
    <div class='row'>
      <div class='column small-12 medium-8'>
        <!-- <small> -->
          <p>
            <?php echo get_theme_mod('full_name') . ' (' . get_theme_mod('short_name') . ')'; ?>
          </p>
          <p>
            <?php echo get_theme_mod('mail_address') ?>
          </p>
          <?php echo "<p>$head_position: <a href=\"$head_url\">$head_title</a>" ?>
        <!-- </small> -->
      </div>
      <div class='column small-12 medium-4'>
        <!-- <small> -->
          <ul class='no-bullet'>
            <li class='menu-item'>
              <a href='/karta-sajta'>Карта сайта</a>
            </li>
            <li class='menu-item'>
              <?php get_search_form() ?>
            </li>
            <li>
              <?php echo social_icons() ?>
            </li>
          </ul>
        <!-- </small> -->
      </div>

    </div>
    <hr>
    <div class="row small-up-1 medium-up-2">
      <div class="column text-center medium-text-left">
        &copy; <?php echo date('Y'); ?> Все права защищены
      </div>
      <div class="column text-center medium-text-right">
        <a class="small-font no-underline" href="//musweb.ru" target="_blank">
          Создание и поддежка сайта - Musweb
        </a>
      </div>
    </div>
  </div>
</footer>
