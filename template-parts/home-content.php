<section class="hello padding-b-xl">
  <!-- первый блок, описание поликлиники-->
  <div class="row column">

      <?php
      if (have_posts()) :
         while (have_posts()) :
            the_post();
               the_content();
         endwhile;
      endif;
      ?>
  </div>
</section>
