<section class="bg-complimentary-dark">
  <div class="row">
    <div class="column small-12">
      <h2>Отделения</h2>
      <p>Отделения поликлиники</p>
    </div>
    <div class="column small-12 large-8">
      <!-- колонка-контейнер для 3 отделений-->
      <div class="row text-center">
        <!-- ряд-контейнер из 3 отделений start-->
        <?php $args = array( 'post_type' => 'mm_department', 'posts_per_page' => 3,'post_parent' => 0, );
          $loop = new WP_Query( $args );
          while ( $loop->have_posts() ) : $loop->the_post(); ?>
          <div class="column small-12 medium-4 padding-on-small padding-bottom">
            <a href="<?php echo get_permalink($post->ID); ?>">
              <?php mm_thumbnail_with_placeholder_if_no_thumbnail("4x3-s", "full-width") ?>
              <div class="bg-accent padding">
                <p class="white-text"><strong><?php the_title(); ?></strong></p>
              </div>
            </a>
          </div>
          <?php endwhile; ?>
      </div>
      <!-- ряд-контейнер из 3 отделений end-->
    </div>
    <!-- колонка-контейнер для 3 отделений-->
    <div class="column small-12 large-4">
      <?php echo get_theme_mod( 'about_departments' ); ?>
    </div>
    <div class="column">
      <a class="button button-with-icon--left margin-top-l" href="<?php echo (get_post_type_archive_link('mm_department')) ?>">
        <i class="fa fa-search fa-fw"></i>
        Посмотреть все отделения
      </a>
    </div>
  </div>
</section>
