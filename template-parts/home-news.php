<section class="bg-grey">
  <!-- блок услуг-->
  <div class="row column">
    <!-- заголовок блока-->
    <h2>Новости и события</h2>
    <p>Ознакомьтесь с последними новостями</p>
  </div>
  <div class="row small-up-1 medium-up-3 large-up-4">
    <!-- ряд-контейнер из 3 отделений start-->
    <?php $args = array( 'post_type' => 'post', 'posts_per_page' => 4 );
    $loop = new WP_Query( $args );

    $i = 1;

    while ( $loop->have_posts() ) : $loop->the_post(); ?>
    <div class="padding-s column padding-on-small text-center padding-bottom <?php

    if ($i == 4) {
      echo 'hide-for-medium-only';
    }

    $i = $i + 1;

    ?>">
      <a class="black-text" href="<?php echo get_permalink($post->ID); ?>">
      <div class="padding-s bg-white">
        <!-- вместо bg-white было border-accent-light  -->
        <div>
          <?php mm_thumbnail_with_placeholder_if_no_thumbnail("1x1-s", "full-width margin-b") ?>
        </div>
        <p class=""><strong><?php the_title(); ?></strong></p>
      </div>
      </a>
    </div>
    <?php endwhile; ?>
  </div>

  <div class="row">
  <div class="column small-12">
    <a class="button button-with-icon--left margin-top-l" href="<?php echo (get_post_type_archive_link('post')) ?>"><i class="fa fa-newspaper-o" aria-hidden="true"></i> Посмотреть все новости</a>
  </div>
  </div>
    <!-- правая колонка-->
  <!-- row-->
</section>
