<?php

$offer_callout = get_theme_mod('home_cta_title');
$offer_desc = get_theme_mod('home_cta_desc');
$offer_service_id = get_theme_mod('home_cta_service');
$offer_service_url = get_permalink( $offer_service_id );

if (empty(get_theme_mod('home_cta_background_img'))):
  $offer_bg_img_id = get_post_thumbnail_id($offer_service_id);
else:
  $offer_bg_img_id = get_theme_mod('home_cta_background_img');
endif;

$bg_obj = new Mm_attachment($offer_bg_img_id);

$offer_link_text = get_theme_mod('home_cta_button_text'); ?>

<!-- <section class="position-relative overflow-hidden padding-tb-0"> -->
<section class="padding-tb-0 offer_height position-relative">
  <?php
  // echo $bg_obj->show('bg-cover-imitation-with-inline-fine-tune-on-y-axis brightness-less', 'full', get_theme_mod('home_offer_bg_img_y_offset'));

  echo $bg_obj->show('object-fit-cover brightness-less z-index-min-1 position-absolute');

   ?>

  <div class="row center-translate-y position-relative">
    <div class="column text-center z-index-100 white-text">
      <h2 class="offer"><?php echo $offer_callout ?></h2>
      <p><?php echo $offer_desc ?></p><a class="button button-with-icon--right bg-third margin-l" href="<?php  echo $offer_service_url ?>"><?php echo $offer_link_text ?><i class="fa fa-check fa-fw"></i></a>
    </div>
  </div>

</section>
