<section class="bg-grey">
  <!-- блок услуг-->
  <div class="row column">
    <!-- заголовок блока-->
    <h2>Избранные услуги</h2>
    <p>Выберите услугу</p>
  </div>
  <div class="row">
    <!-- контейнер на две колонки-->
    <div class="column medium-12 large-12">
      <!-- колонка трех услуг с подписью внизу-->
      <div class="row">

      <?php
      $org = new Mm_org();
      $services = $org->services(6);

      // $i = 0;

      foreach ($services as $service):
        // $i = $i + 1; ?>

        <div class="padding-s column small-6 medium-3 large-2 text-center">
        <?php
        // картинка смотрится хорошо с шириной 100%,
        // а иконка - с шириной 70%
        //этому и посвящен сей workaround

        if ($service->has_thumbnail()):
          if ($service->thumbnail()->mime() == "image/svg+xml") {
            $cl = "almost-full-width";
          } else {
            $cl = "full-width";
          }
        endif; ?>

        <a class="black-text" href="<?php echo $service->url(); ?>">
          <!-- задача -
          1) квадратные контейнеры
          2)  -->
          <div class="fixed-proportions-box hover-effect">    <!-- margin-b -->
            <div class="fixed-proportions-content center-vertically bg-white circle">
                <?php echo $service->show_thumbnail("<?php echo $cl ?> no-more-than-container", "1x1") ?>
            </div>
          </div>
          <p class="text-center padding-tb-s"><strong><?php echo $service->title() ?></strong></p>
        </a>
        </div>

      <?php endforeach; ?>

        <div class="column small-12"> <!-- текст под иконками услуг -->
          <a class="button button-with-icon--left margin-top-l" href="<?php echo (get_post_type_archive_link('mm_service')) ?>"><i class="fa fa-search fa-fw"></i> Посмотреть все услуги</a>
        </div>
      </div>
    </div>

  <!-- row-->
</section>
