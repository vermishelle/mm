<div class="row column">
  <div class="row text-center small-up-2 medium-up-4 welcome-blocks">
    <a <?php if(is_ext_url(get_theme_mod('block1_url'))): echo 'target="_blank"'; endif; ?> class="padding-l bg-accent-darkest column magnification" href="<?php echo get_theme_mod('block1_url') ?>">
      <i class="fa fa-<?php echo get_theme_mod('block1_icon') ?> fa-4x hero-icon"></i>
      <p>
        <strong class="uppercase"><?php echo get_theme_mod('block1_title') ?></strong>
      </p>
      <p class='show-for-medium'><?php echo get_theme_mod('block1_desc') ?></p>
    </a>
    <a <?php if(is_ext_url(get_theme_mod('block2_url'))): echo 'target="_blank"'; endif; ?> class="padding-l bg-accent-dark column magnification" href="<?php echo get_theme_mod('block2_url') ?>">
      <i class="fa fa-<?php echo get_theme_mod('block2_icon') ?> fa-4x hero-icon"></i>
      <p>
        <strong class="uppercase"><?php echo get_theme_mod('block2_title') ?></strong>
      </p>
      <p class='show-for-medium'><?php echo get_theme_mod('block2_desc') ?></p>
    </a>
    <a <?php if(is_ext_url(get_theme_mod('block3_url'))): echo 'target="_blank"'; endif; ?> class="padding-l bg-accent column magnification" href="<?php echo get_theme_mod('block3_url') ?>">
      <i class="fa fa-<?php echo get_theme_mod('block3_icon') ?> fa-4x hero-icon"></i>
      <p>
        <strong class="uppercase"><?php echo get_theme_mod('block3_title') ?></strong>
      </p>
      <p class='show-for-medium'><?php echo get_theme_mod('block3_desc') ?></p>
    </a>
    <a <?php if(is_ext_url(get_theme_mod('block4_url'))): echo 'target="_blank"'; endif; ?> class="padding-l bg-accent-light column magnification" href="<?php echo get_theme_mod('block4_url') ?>">
      <i class="fa fa-<?php echo get_theme_mod('block4_icon') ?> fa-4x hero-icon"></i>
      <p>
        <strong class="uppercase"><?php echo get_theme_mod('block4_title') ?></strong>
      </p>
      <p class='show-for-medium'><?php echo get_theme_mod('block4_desc') ?></p>
    </a>
  </div>
</div>
