<?php
$light_logo = get_theme_mod('logo_light_on_dark');
$dark_logo = get_theme_mod('logo_dark_on_light');
$light_logo_obj = new Mm_attachment($light_logo);
$dark_logo_obj = new Mm_attachment($dark_logo);

$org = new Mm_org();
$enquiry = $org->enquiry();
$emergency = $org->emergency();
$visit = $org->visit();
$hotline = $org->hotline();
?>

<div class="bg-complimentary-dark padding-tb-s">
  <div class="row font-size-smaller line-height--bigger font-color-grey">
    <!-- description -->
    <div class="column small-12 medium-6 text-center medium-text-left">
      <?php bloginfo( "description" ); ?>
    </div>

    <!-- logo -->
    <div class="column small-5 hide-for-medium text-center">
      <a href="<?php bloginfo( 'url' ); ?> ">
        <?php echo $light_logo_obj->show('logo--extratop'); ?>
      </a>
    </div>

    <!-- visually impaired -->
    <div class="column small-7 medium-6 text-right toggle-low-vision font-color-accent-light">
      <i title='Версия для слабовидящих' class="fa fa-low-vision margin-lr-s" aria-hidden="true"></i>
      <span>Версия для слабовидящих</span>
      <!-- <span> | </span>
      <a title='Поиск' href="<?php bloginfo("url") ?>/poisk/">
        <i class="fa fa-search margin-lr-s font-color-accent-light" aria-hidden="true"></i> Поиск
      </a>
      <span> | </span>
      <a title='Карта сайта' href="<?php bloginfo("url") ?>/karta-sajta/">
        <i class="fa fa-sitemap margin-lr-s font-color-accent-light" aria-hidden="true"> Карта сайта</i>
      </a> -->
    </div>
  </div>
</div>

<?php $menu_args = array(
  'theme_location'  => 'header-menu',
  'menu'            => 'Main',
  'container'       => 'nav',
  'container_class' => 'main-menu',
  'container_id'    => '',
  'menu_class'      => 'mm_menu uppercase no-bullet margin-b-0',
  'menu_id'         => '',
  'echo'            => true,
  'fallback_cb'     => 'wp_page_menu',
  'before'          => '',
  'after'           => '',
  'link_before'     => '',
  'link_after'      => '',
  'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
  'depth'           => 1,
  'walker'          => ''
); ?>

<div class="row align-middle">
  <div class='show-for-medium column small-12 medium-3 text-center'>
    <a class="" href="<?php echo get_bloginfo( 'url' ) ?>">
      <?php echo $dark_logo_obj->show('logo--top'); ?>
    </a>
  </div>
  <div class='column small-12 medium-9'>
    <div class='row'>
      <div class='column small-12 medium-6'>
        <?php if ($enquiry != NULL): ?>
          <p class="margin-tb-s"><strong><?php echo $enquiry->title(); ?></strong>: <?php echo str_replace('-', '&#8209;', $enquiry->phone_number()); ?></p>
        <?php endif; ?>
      </div>
      <div class='column small-12 medium-6'>
        <?php if ($visit != NULL): ?>
          <p class="margin-tb-s"><strong><?php echo $visit->title();?></strong>: <?php echo str_replace('-', '&#8209;', $visit->phone_number());?></p>
        <?php endif; ?>
      </div>
    </div>
    <div class='row'>
      <div class='column small-12 medium-6'>
        <?php if ($hotline != NULL): ?>
          <p class="margin-tb-s"><strong><?php echo $hotline->title();?></strong>: <?php echo str_replace('-', '&#8209;', $hotline->phone_number()); ?></p>
        <?php endif; ?>
      </div>
      <div class='column small-12 medium-6'>
        <?php if ($emergency != NULL): ?>
          <p class="margin-tb-s"><strong><?php echo $emergency->title();?></strong>: <?php echo str_replace('-', '&#8209;', $emergency->phone_number()); ?></p>
        <?php endif; ?>
      </div>
    </div>
  </div>
  <div class='column small-12'>
    <hr  class="margin-tb-s">
  </div>
  <div class='column small-12 font-color-white'>
    <?php wp_nav_menu($menu_args); ?>
  </div>
</div>
